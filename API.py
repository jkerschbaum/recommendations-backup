from src.comm.io_api import rec_init, rec_new_data, rec_new_metadata, \
    rec_empty_datacarriage, rec_get_recs, rec_on_shutdown
from src.comm.io_api import start_auto_updater as start_auto
from src.comm.io_api import end_auto_updater as end_auto


def initialise(num_of_recs=5):
    """
        Initialises all recommendation functionality

        Keyword arguments:
            num_of_recs: the number of recommendations to be prepared
                         (default: 5)

        Return Values:
            True: Init succeeded
            False or None: Init failed

        Side Effects:
            -

        Possible Exceptions:
            -

        Restriction:
            Call only once, at setup
    """
    return rec_init(num_of_recs)


def new_dsl_string(dsl_string):
    """
        Takes in new (non-met_mod) data and updates the internal state

        Keyword arguments:
            dsl_string: dsl as entered by user, to be used for predictions
                        (non-optional)

        Return Values:
            True: Succeeded
            False or Init: Failed

        Side Effects:
            -

        Possible Exceptions:
            -

        Restrictions:
            -
    """
    return rec_new_data(dsl_string)


def new_metadata(dc, update=False):
    """
        Takes in new met_mod-data in the form of a DataCarriage Object and
        updates the internal state

        Keyword arguments:
            dc: DataCarriage Object used to draw new metadata from
                (non-optional)

        Return Values:
            True: Succeeded
            False or None: Failed

        Side Effects:
            -

        Possible Exceptions:
            -

        Restrictions:
            -
    """
    return rec_new_metadata(dc, update)


def empty_dataccarriage():
    """
        Returns empty DataCarriage Object for collection and updating of
        Metadata variables

        Keyword arguments:
            -

        Return Values:
            dc: Empty DataCarriage Object for collection and updating of
                metadata

        Side Effects:
            -

        Possible Exceptions:
            -

        Restrictions:
            -
    """
    return rec_empty_datacarriage()


def get_recs():
    """
        Gets prepared Recs from the buffer, as prepared from last update

        Keyword arguments:
            -

        Return Values:
            ret_recs: List of Strings in DSL-Code-Format that represent
                      predicted behaviour

        Side Effects:
            -

        Possible Exceptions:
            -

        Restrictions:
            -
    """
    return rec_get_recs()


def start_auto_updater(func_timer, func_target):
    """
            Starts a new thread that automatically updates the recommendations
            every func_timer minutes and pushes them
            into func_target. func_target is assumed to be a list, and must
            implement the __setitem__ and append functions
            for this to work.

            For ideal functionality, func_target should be a single, empty list
            that is not assigned to, ever. At all.

            Due to reasons of Python, the recommendation list will not be
            assigned to func_target, but rather to func_target[0].

            Keyword arguments:
                func_timer: (Whole) Number, representing the time in minutes
                            ideally elapsing between updates
                func_target: Caller-side variable (reference) to push updated
                recs into.

            Return Values:
                -

            Side Effects:
                Changes func_target variables value at relatively consistent
                intervalls

            Possible Exceptions:
                -

            Restrictions:
                -
    """
    return start_auto(func_timer, func_target)


def end_auto_updater(id):
    """
            Shuts down an auto-updater thread. The thread may perform up to one
            more full update after this is called.

            Keyword arguments:
                id: id of the thread to be killed

            Return Values:
                -

            Side Effects:
                -

            Possible Exceptions:
                -

            Restrictions:
                -
    """
    end_auto(id)


def shutdown():
    """
       Shutdown function. Cleans up various bits and pieces, updates long-term
       evidence storage

       Keyword arguments:
           -

       Return Values:
           -

       Side Effects:
           Changes .csv files used for long-term evidence storage

       Possible Exceptions:
           -

       Restrictions:
           Only call on shutdown
    """
    rec_on_shutdown()
