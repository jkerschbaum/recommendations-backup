# recommendations

This project is part of the OpenTable project. The idea is to provide reasonable recommendations for next actions of the user. This is achieved through a mixture of hardcoded and learned recommendations based on actions performed by the user.

[[_TOC_]]

## Usage

```python
import recommendations.API as rec_api
```

### API functions

#### Initialise

```python
rec_api.initialise(num_of_recs)
```

Initialises all recommendation functionality. Call only once, ideally at startup.\
Argument num_of_recs gives the amount of recommendations to be prepared per request.\
Algorithms aim to provide the num_of_recs best possible recommendation.
Also reads long-term storage for evidence for learned models from disk if extant.

#### New DSL String

```python
rec_api.new_dsl_string(dsl_string)
```

Provides a new string in DSL format to the internal state. Later used to provide examples for learned recommendation models.
The implementation assumes there is no overlap between dsl_string and previously provided DSL code.\
Sets the DATA_UPDATED flag on call.

#### New Metadata

```python
rec_api.new_metadata(dc, update)
```

Provides new Metadata in the format of a DataCarriage object for the internal state. Primarily used for hardcoded recommendation modelling.
dc must be a DataCarriage. update is an optional argument, defaults to False if not provided. 
If update is set to True, then the metadata provided from the last call is preserved in addition to the newly provided.\
Sets the DATA_UPDATED flag on call.

#### Empty DataCarriage

```python
rec_api.empty_datacarriage()
```

Provides an empty DataCarriage Object for the caller to fill and to return MetaData with.

#### Get Recs

```python
rec_api.get_recs()
```

Returns a unified list of hardcoded and learned recommendations. If the DATA_UPDATED flag is set, runs the updating procedure first,
providing fresh recommendations when neccessary while keeping repeated requests quick.
Sets DATA_UPDATED flag to False after call.\
See Method of Operation for further details.

#### Start Auto-updater

```python
rec_api.start_auto_updater(timer, target)
```

Starts an autonomous updater that writes updated recs to variable target every timer minutes, including if no change occurred.\
For technical reasons, func_target should be a python-builtin list, or any other class that implements .append() and .__ setitem __ in a similar fashion.\
Note that the current implementation expressely performs no checks on targets type in order to ease use of custom classes.\
Variable target should not ever be written to by any function other than the auto-updater, nor should target be deleted, or otherwise derefrenced while the auto-updater is still running.
Doing so will most likely cause some sort of Exception and/or leave artefact data on target.

Returns an ID, which acts as a unique identifier for the auto-updater and can be used to shut it down preemptively.

#### End Auto-updater

```python
rec_api.end_auto_updater(id)
```

Ends the auto-updater with the ID id, as provided upon starting.\
This may lead to significant overhead, as depending on the exact timing of the call, the auto-updater may conclude another full update cycle including sleep time according to the provided timer.
This issue is inherently unsolveable due to the architecture of the underlying models, evaluations must be performed in full to avoid artifacts.\
Note that each auto-updater must be ended individually, and that each should be ended as soon as reasonable.
Each unended auto-updater may extend the shutdown sequence, especially if long timers are employed.


#### Shutdown

```python
rec_api.shutdown()
```

Provides shutdown procedure, including preparation of long-term storage of evidence for use by learned models in the future.\
Ends any currently-running autoupdaters.\
Always call only once, immediately prior to the (planned) shutdown of the application.

## Method of Operation

```mermaid
graph TD
    A[New Data] -->|Permanently set, not immediate| B{Internal State}
    C[New Metadata] -->|Permanently set, not immediate| B
    B -.-> D("IO_API.rec_get_recs()")
    D -->|Metadata| E[Hardcoded]
    D -->|Data| F[Learned]
    F --> G[Frequency Analysis]
    F --> H[Transition Analysis]
    G --> I(Unify by MetaModel)
    H --> I 
    E --> J(Unify by deterministic Algorithm)
    I --> J
    J --> K[Translate to DSL code]
    K --> L(Return)
    M(External Call) --> D
    style M stroke-dasharray: 5.5
    style B stroke-dasharray: 5.5
```

New Data and Metadata are provided by external users of the package through the APIs rec_new_data and rec_new_metadata functions.
This data is used to update the internal state and, upon external request, is fed into the internal recommendation update pipeline.\
Recommendations are constructed through a mix of hardcoded and learned systems, with the learned processes being further subdivided into frequency analysis and transition probabillity analysis.
The recommendations produced by these processes are unified into a single list in a two-step process, translated to DSL code and returned to the caller.

### Hardcoded Recommendations

```mermaid
graph TD
    A[Data Carriage] --> B[Key] -->|Association Dictionary| C[Reccommendation Key] -->|Recommendations Dictionary| D[List of Hardcoded Recs] -.-> E(".initialize()")
    D --> F[Return]
```

From the DataCarriage Object that represents the MetaData considered relevant, a key is created such that each key is unique.
The list of recommendations to make for this key is obtained from the Association Dictionary.
The list of actual Recommendation Objects is gotten from the Recommendations Dictionary by the list of recommendations to make.
The Recommendation Objects are initialised as neccessary, and returned to the next higher level for unification.

### Learned Recommendations

#### Frequency Analysis

Through the total amount of actions taken and the amount of times each specific action was taken, the overall frequency / probability of any given action is calculated.
The overall probability of the known sequence is calculated, and used as a basis to determine the probability of each possible follow-up action.
The possible follow-up actions with the highest probabilities are returned for unification.
Currently supports probabilities of one-long, two-long and three-long sublists.

#### Transition Probability Analysis

The total amount of two-long sublists of all types, classified by first and second element, is counted, and for each the type is noted.
A modified version of Laplace Smoothing is applied to minimise errors from very small datasets.
A Problog model is used to learn and get the probabilities of every given sublist.
The overall probability of the known sequence is calculated, and used as a basis to determine the probability of each possible follow-up action.
The possible follow-up actions with the highest probabilities are returned for unification.

### Unification Processes

#### Unification by MetaModel

A Problog Model is used to evaluate the absolute and relative probabilities of either no, or both learned processes providing correct information.
The lists of recommendations provided by these processes are retained for this purpose, along with the actual action taken.
From this, the evidence to train the model is built.
The recommendations are unified in accordance with the relative difference in probabilities.

#### Unification by Deterministic Algorithm

The lists of learned and hardcoded recommendations are unified by a fully deterministic (and non-learned) algorithm that uses a sigmoid function as a guide,
such that with an increase of evidence obtained in the process of execution,
the amount of learned recommendations included in the final list increases compared to the amount of hardcoded recommendations.\
Specifically, the distribution between the hardcoded and learned recommendations is dictated by the function

```math
(e\raisebox{0.25em}{(sf * (ui - sr))} - 1) / (2 * (1 + e\raisebox{0.25em}{(sf * (ui - sr))})) + su
```

Where sf (stretch factor) is currently 0.02, sr (shift right) is currently 70, and su (shift up) is currently 0.5.
Ui is the 'usage indicator', a parameter that is calculated from the amount of examples collected for the learning models to work with.
Ui is generally going to increase throughout operation, meaning that more learned recommendations will be introduced into the output as time passes.
