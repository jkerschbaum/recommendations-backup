This file notes relevant points towards the file contained in the /docs folder, in so far as they are not self-evident.

The subfolder /docs/stuff-not-used provides partial implemntations for features / processes that were intended for the module, but scrapped for one reason or another.

Please note that the /docs folderand its subfolder aren't part of the project "proper", and mostly exist due to its nature as a uni practial.

----
Demo Explanation:

    Execute demo.py

    Button "Execute Test Step":                         Executes a single test step (input dsl string + datacarriage and print result)
    Button "Execute All Test Steps":                    Executes all test steps immediately
    Button "Get Results without further Test Step":     Immediately prints result
    Checkbox "Toggle Endless Mode":                     If 1, then Test Steps will loop
    Menu "Manual Input":                                Provides for manual input of dsl strings or datacarriages
    Menu "Readouts":                                    Provides for readout of various relevant variables
