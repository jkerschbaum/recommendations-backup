import unittest
from docs.stuff_not_used.problem_class import *


class input_handling_tests(unittest.TestCase):
    def test_NOTHING_1(self):
        cells = []
        metadata = [1, 1]
        self.assertEqual(ProblemClass.NOTHING, get_problem_class(cells,
                                                                 metadata))

    def test_ERROR_1(self):
        cells = [[1, 1], [1, -1]]
        metadata = [5, 5]
        self.assertEqual(ProblemClass.ERROR, get_problem_class(cells,
                                                               metadata))

    def test_ERROR_2(self):
        cells = [[1, 4]]
        metadata = [1, 1]
        self.assertEqual(ProblemClass.ERROR, get_problem_class(cells,
                                                               metadata))

    def test_A_1(self):
        cells = [[1, 4]]
        metadata = [5, 5]
        self.assertEqual(ProblemClass.A, get_problem_class(cells, metadata))

    def test_B1_1(self):
        cells = [[1, 1], [2, 1], [3, 1]]
        metadata = [4, 3]
        self.assertEqual(ProblemClass.B1, get_problem_class(cells, metadata))

    def test_B2_1(self):
        cells = [[2, 1], [2, 3], [2, 2]]
        metadata = [4, 4]
        self.assertEqual(ProblemClass.B2, get_problem_class(cells, metadata))

    def test_B3_1(self):
        cells = [[1, 1], [2, 2], [3, 3]]
        metadata = [3, 3]
        self.assertEqual(ProblemClass.B3, get_problem_class(cells, metadata))
    
    def test_C1_1(self):
        cells = [[1, 1], [1, 2], [1, 3]]
        metadata = [8, 3]
        self.assertEqual(ProblemClass.C1, get_problem_class(cells, metadata))

    def test_C2_1(self):
        cells = [[1, 1], [1, 2], [1, 3], [2, 1], [2, 2], [2, 3], [4, 1],
                 [4, 2], [4, 3]]
        metadata = [4, 3]
        self.assertEqual(ProblemClass.C2, get_problem_class(cells, metadata))

    def test_D1_1(self):
        cells = [[1, 13], [2, 13], [3, 13], [4, 13]]
        metadata = [4, 15]
        self.assertEqual(ProblemClass.D1, get_problem_class(cells, metadata))

    def test_D2_1(self):
        cells = [[1, 3], [2, 3], [3, 3], [1, 1], [2, 1], [3, 1]]
        metadata = [3, 8]
        self.assertEqual(ProblemClass.D2, get_problem_class(cells, metadata))


if __name__ == '__main__':
    unittest.main()
