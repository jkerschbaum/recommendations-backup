from enum import Enum
import numpy as np


class ProblemClass(Enum):
    NOTHING = -1    # User selected nothing
    A = 0           # User selected one cell
    B1 = 1          # User selected multiple cells form different rows
    B2 = 2          # User selected multiple cells form different cols
    B3 = 3          # User selected multiple cells form different rows and cols
    C1 = 4          # User selected a single row
    C2 = 5          # User selected multiple rows
    D1 = 6          # User selected a single column
    D2 = 7          # User selected multiple columns
    ERROR = 8       # Input incorrect


def get_problem_class(cells, metadata):
    """
        This function returns the problem class given selected cells and
        the amount of rows and columns of the table.

        Keyword arguments:
            cells: Numpy array of indices of selected cells.
            metadata: Array of [existing_rows, existing_cols]

        Return Values:
            result: Problem_class of the selected input

        Side effects:
            -

        Possible Exception:
            -

        Restrictions:
            -
        """    
    
    # Define variables
    amount_cells = len(cells)
    amount_rows = metadata[0]
    amount_cols = metadata[1]

    # Additional properties of rows and cols for further calculations
    row_index, col_index = get_row_and_col_min_max_indices(cells,
                                                           amount_cells)
    complete_rows_selected = find_complete_rows_or_cols(cells, amount_cells,
                                                        amount_rows,
                                                        amount_cols, 0)
    complete_cols_selected = find_complete_rows_or_cols(cells, amount_cells,
                                                        amount_rows,
                                                        amount_cols, 1)
    result = catch_error_problem_class(row_index, col_index, amount_cells, 
                                       amount_rows, amount_cols)
    if result is not None:
        return result

    result = catch_NOTHING_A_problem_class(amount_cells)
    if result is not None:
        return result

    result = catch_multiple_cells_problem_class(row_index, col_index,
                                                amount_cells, amount_rows,
                                                amount_cols,
                                                complete_rows_selected,
                                                complete_cols_selected)
    if result is not None:
        return result


def find_complete_rows_or_cols(cells, amount_cells, amount_rows, amount_cols,
                               row_or_col):
    """
        This function detects if multiple rows or columns are selected and
        returns it as a int.

        Keyword arguments:
            cells: Selected cells.
            amount_cells: Amount of selected cells.
            amount_rows: Existing rows.
            amount_cols: Existing cols.
            row_or_col: 0 yields row information, 1 yields col information.

        Return Values:
            Number of completely selected rows or columns.

        Side effects:
            -

        Possible Exception:
            -

        Restrictions:
            -
    """
    if row_or_col == 0:
        amount_range = amount_cols
        amount_count = amount_cols
    elif row_or_col == 1:
        amount_range = amount_rows
        amount_count = amount_rows

    indices = []
    complete_row_or_col_counter = 0

    for i in range(amount_cells):
        indices.append(cells[i][row_or_col])

    for i in range(amount_range + 1):
        if indices.count(i) == amount_count:
            complete_row_or_col_counter += 1
    return complete_row_or_col_counter


def get_row_and_col_min_max_indices(cells, amount_cells):
    """
        This function returns the the min/max index of rows and cols of cells
        in a dictionary.

        Keyword arguments:
            cells: Numpy array of indices of selected cells.
            amount_cells: Amount of cells

        Return Values:
            row_data: min/max index of rows in cells
            col_data: min/max index of cols in cells

        Side effects:
            -

        Possible Exception:
            -

        Restrictions:
            -
    """
    # Calculate min/max index
    if amount_cells == 0:
        row_data = {"min": 0, "max": 0}
        col_data = {"min": 0, "max": 0}
        return row_data, col_data
    else:
        row_data = []
        col_data = []
        for i in range(0, amount_cells):  # get row and col data
            row_data.append(cells[i][0])
            col_data.append(cells[i][1])

        # Convert to np arrays
        row_data = np.array(row_data)
        col_data = np.array(col_data)

        row_data = {"min": np.min(row_data), "max": np.max(row_data)}
        col_data = {"min": np.min(col_data), "max": np.max(col_data)}
        return row_data, col_data


def catch_error_problem_class(row_index, col_index, amount_cells, amount_rows,
                              amount_cols):
    """
        This function checks if the Problem_class is ERROR

        Keyword arguments:
            row_index: Min/Max index of rows
            col_index: Min/Max index of cols
            amount_cells: Amount of cells
            amount_rows: Amount of rows
            amount_cols: Amount of columns

        Return Values:
            Problem_class.ERROR or None

        Side effects:
            -

        Possible Exception:
            -

        Restrictions:
            -
    """
    if amount_cells < 0:
        return ProblemClass.ERROR
    elif row_index["max"] > amount_rows or row_index["min"] < 0:
        return ProblemClass.ERROR
    elif col_index["max"] > amount_cols or col_index["min"] < 0:
        return ProblemClass.ERROR


def catch_NOTHING_A_problem_class(amount_cells):
    """
        This function checks if the Problem_class is NOTHING or A

        Keyword arguments:
            amount_cells: Amount of cells

        Return Values:
            Problem_class.NOTHING or Problem_class.A or None

        Side effects:
            -

        Possible Exception:
            -

        Restrictions:
            -
    """
    if amount_cells == 0:
        return ProblemClass.NOTHING
    elif amount_cells == 1:
        return ProblemClass.A


def catch_multiple_cells_problem_class(row_index, col_index, amount_cells,
                                       amount_rows, amount_cols,
                                       complete_rows_selected,
                                       complete_cols_selected):
    """
        This function checks if the Problem_class is of any multi-cell
        selection Problem_class (B1-B3, C1-C2, D1-D2)

        Keyword arguments:
            row_index: Min/Max index of rows
            col_index: Min/Max index of cols
            amount_cells: Amount of cells
            amount_rows: Amount of rows
            amount_cols: Amount of columns
            complete_rows_selected: How many complete rows are selected
            complete_cols_selected: How many complete columns are selected

        Return Values:
            Problem_class.B1 or Problem_class.B2 or Problem_class.B3 or
            Problem_class.C1 or Problem_class.C2 or
            Problem_class.D1 or Problem_class.D2

        Side effects:
            -

        Possible Exception:
            -

        Restrictions:
            -
    """
    if row_index["max"] == row_index["min"] and amount_cells == amount_cols:
        return ProblemClass.C1
    elif row_index["max"] == row_index["min"]:
        return ProblemClass.B2
    elif col_index["max"] == col_index["min"] and amount_cells == amount_rows:
        return ProblemClass.D1
    elif col_index["max"] == col_index["min"]:
        return ProblemClass.B1

    # Cases for multiple rows and cols
    if complete_rows_selected:
        return ProblemClass.C2
    elif complete_cols_selected:
        return ProblemClass.D2
    else:
        return ProblemClass.B3
