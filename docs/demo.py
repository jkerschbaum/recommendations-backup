from tkinter import *
from tkinter import messagebox
from tkinter import simpledialog
#import API
import time

import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import API
from src.data.atomic_actions import data
from src.comm.io_api import get_data_updated

test_dsl = ["replace(arg1, arg2).replace(arg2, arg3)", "trim()", None, "extractAll(remove(arg1), arg2)"] * 10
empty_dc = API.empty_dataccarriage()
dc1 = API.empty_dataccarriage()
dc1.created_column = True
test_dc = [empty_dc, dc1, dc1, dc1] * 10
test_update = [False, False, True, False] * 10

# Button: Execute a single test step, execute all steps
# Cumulative testing -> Shows projects strenghts, easier
# Labels: Current test num, current test string, current test dc true vars +  is update, last returned recs

class DCInputDialog(simpledialog.Dialog):
	def __init__(self, parent):
		self.mainwindow = parent
		super().__init__(parent, "Manual Input of Metadata through DataCarriage")
		
	def body(self, master):
		self.geometry("400x250")
		#creating vars
		self.createdColumnVar = IntVar()
		self.selectedSingleCellVar = IntVar()
		self.selectedMultipleCellsVar = IntVar()
		self.selectedEntireRowVar = IntVar()
		self.selectedSingleColumnVar = IntVar()
		self.selectedMultipleColumnsVar = IntVar()
		#creating boxes
		self.createdColumnBox = Checkbutton(self, text="Created Column", variable=self.createdColumnVar, onvalue=1, offvalue=0, command=debug)
		self.selectedSingleCellBox = Checkbutton(self, text="Selected Single Cell", variable=self.selectedSingleCellVar, onvalue=1, offvalue=0, command=debug)
		self.selectedMultipleCellsBox = Checkbutton(self, text="Selected Multiple Cells", variable=self.selectedMultipleCellsVar, onvalue=1, offvalue=0, command=debug)
		self.selectedEntireRowBox = Checkbutton(self, text="Selected Entire Row", variable=self.selectedEntireRowVar, onvalue=1, offvalue=0, command=debug)
		self.selectedSingleColumnBox = Checkbutton(self, text="Selected Single Column", variable=self.selectedSingleColumnVar, onvalue=1, offvalue=0, command=debug)
		self.selectedMultipleColumnsBox = Checkbutton(self, text="Selected Multiple Colums", variable=self.selectedMultipleColumnsVar, onvalue=1, offvalue=0, command=debug)
		#placing stuff
		self.createdColumnBox.place(x=100, y=20)
		self.selectedSingleCellBox.place(x=100, y=40)
		self.selectedMultipleCellsBox.place(x=100, y=60)
		self.selectedEntireRowBox.place(x=100, y=80)
		self.selectedSingleColumnBox.place(x=100, y=100)
		self.selectedMultipleColumnsBox.place(x=100, y=120)
		return
	
	def buttonbox(self):
		self.okButton = Button(self, text="Input", command=self.okButton)
		self.cancelButton = Button(self, text="Cancel", command=self.cancelButton)
		self.okButton.place(x=100, y=200)
		self.cancelButton.place(x = 220, y=200)
		return
	
	def okButton(self):
		returned_dc = API.empty_dataccarriage()
		if self.createdColumnVar.get() == 1:
			returned_dc.created_column = True
		if self.selectedSingleCellVar.get() == 1:
			returned_dc.selected_single_cell = True
		if self.selectedMultipleCellsVar.get() == 1:
			returned_dc.selected_multiple_cells = True
		if self.selectedEntireRowVar.get() == 1:
			returned_dc.selected_entire_row = True
		if self.selectedSingleColumnVar.get() == 1:
			returned_dc.selectedSingleColumn = True
		if self.selectedMultipleColumnsVar.get() == 1:
			returned_dc.selected_multiple_columns = True
		API.new_metadata(returned_dc)
		self.destroy()
	
	def cancelButton(self):
		self.destroy()
		

class Window(Frame):
	def __init__(self, master=None):
		Frame.__init__(self, master)
		self.master = master
		self.pack(fill=BOTH, expand=1)
		API.initialise()
		self.testNumber = 0
		self.timeTaken = 0
		self.testNumberLabel = Label(self, text="Next Test is number {}".format(self.testNumber))
		self.testNumberLabel.place(x=505, y=10)
		self.executeStepButton = Button(self, text="Execute Test Step", command=self.testStep)
		self.executeStepButton.place(x=510, y=50)
		self.executeAllButton = Button(self, text="Execute All Test Steps", command=self.testAll)
		self.executeAllButton.place(x=500, y=100)
		self.noTestButton = Button(self, text="Get Results without\n further Test Step", command=self.getResult)
		self.noTestButton.place(x=500, y=150)
		self.test_dslLabel = Label(self, text="The entered DSL-code will be displayed here")
		self.test_dslLabel.place(x=50, y=50)
		self.test_dcLabel = Label(self, text="The entered Metadata will\nbe displayed here")
		self.test_dcLabel.place(x=50, y=100)
		self.resultLabel = Label(self, text="The result / predictions\nwill be displayed here\n(Note that, for accuracys sake,\nno recommendations will be provided\nwith less than 3 data points.)")
		self.resultLabel.place(x=260, y=170)
		self.timeTakenLabel = Label(self, text="")
		self.timeTakenLabel.place(x=50, y=350)
		self.endlessVar = IntVar()
		self.endlessBox = Checkbutton(self, text="Toggle Endless Mode", variable=self.endlessVar, onvalue=1, offvalue=0, command=debug)
		self.endlessBox.place(x=10, y=10)
	
	def testStep(self):
		if self.testNumber == len(test_dsl):
			return
		if test_dsl[self.testNumber] is not None:
			API.new_dsl_string(test_dsl[self.testNumber])
			self.test_dslLabel.config(text=test_dsl[self.testNumber])
		else:
			self.test_dslLabel.config(text="None")
		if test_dc[self.testNumber] is not None:
			API.new_metadata(test_dc[self.testNumber], test_update[self.testNumber])
			labelText = ""
			for i in [attr for attr in dir(test_dc[self.testNumber]) if not callable(getattr(test_dc[self.testNumber], attr)) and not attr.startswith('__')]:
				labelText += "{}: {}\n".format(i, getattr(test_dc[self.testNumber], i))
			self.test_dcLabel.config(text=labelText)
		else:
			self.test_dcLabel.config(text="None")
		t0 = time.time()
		result = API.get_recs()
		t1 = time.time()
		resultLabelText = ""
		for i in result:
			resultLabelText += i
			resultLabelText += "\n"
		self.resultLabel.config(text=resultLabelText)
		self.testNumber += 1
		self.testNumberLabel.config(text="Next Test is number {}".format(self.testNumber))
		if self.testNumber == len(test_dsl)-1:
			self.testNumberLabel.config(text="Next Test is the last")
		elif self.testNumber == len(test_dsl):
			if self.endlessVar.get() == 0:
				self.testNumberLabel.config(text="No more test steps")
			else:
				self.testNumber = 0
				self.testNumberLabel.config(text="Next Test is number {}".format(self.testNumber))
		self.timeTakenLabel.config(text="Getting that result took {:.4}s".format(t1 - t0))
		return
	
	def testAll(self):
		for i in range(self.testNumber, len(test_dsl)):
			print(i)
			if test_dsl[i] is not None:
				API.new_dsl_string(test_dsl[i])
			if test_dc[i] is not None:
				API.new_metadata(test_dc[i], test_update[i])
		t0 = time.time()
		result = API.get_recs()
		t1 = time.time()
		print(result)
		self.testNumber = len(test_dsl)-1
		resultLabelText = ""
		for i in result:
			resultLabelText += i
			resultLabelText += "\n"
		self.resultLabel.config(text=resultLabelText)
		if self.endlessVar.get() == 0:
			self.testNumberLabel.config(text="No more test steps")
		else:
			self.testNumber = 0
			self.testNumberLabel.config(text="Next Test is number {}".format(self.testNumber))
		self.timeTakenLabel.config(text="Getting that result took {:.4}s".format(t1 - t0))
		return
		
	def commandResetCounter(self):
		self.testNumber = 0
		self.testNumberLabel.config(text="Next Test is number {}".format(self.testNumber))
	
	def getResult(self):
		t0 = time.time()
		result = API.get_recs()
		t1 = time.time()
		resultLabelText = ""
		for i in result:
			resultLabelText += i
			resultLabelText += "\n"
		self.resultLabel.config(text=resultLabelText)
		self.timeTakenLabel.config(text="Getting that result took {:.4}s".format(t1 - t0))
		return


def debug():
   return
  
def commandExit():
	API.shutdown()
	root.quit()

def commandAbout():
	messagebox.showinfo("Title", "About")

def commandAllTests():
	tests = test_dsl
	for i in range(len(tests)):
		if tests[i] is None:
			tests[i] = "None"
	messagebox.showinfo("All Tests", "\n".join(tests))

def stringInputTest():
	result = simpledialog.askstring(title="Manual DSL-String input", prompt="")
	API.new_dsl_string(result)
	
def dcInput():
	DCInputDialog(app)

def readoutDataCommand():
	messagebox.showinfo("Readout len(data))", "{}".format(len(data)))

def readoutDataUpdatedCommand():
	messagebox.showinfo("Readout Data Updated Flag", "{}".format(str(get_data_updated())))

root = Tk()
app = Window(root)
root.wm_title("recommendations project test gui")
root.geometry("640x400")
menubar = Menu(root)
menu = Menu(menubar, tearoff=0)
menu.add_command(label="Reset counter", command=app.commandResetCounter)
menu.add_command(label="Display all tests", command=commandAllTests)
menu.add_command(label="About", command=commandAbout)
menu.add_command(label="Exit", command=commandExit)
menubar.add_cascade(label="Menu", menu=menu)
second_menu = Menu(root, tearoff = 0)
second_menu.add_command(label="String Input", command=stringInputTest)
second_menu.add_command(label="DC Input", command=dcInput)
menubar.add_cascade(label="Manual Input", menu=second_menu)
third_menu = Menu(root, tearoff = 0)
#commands
third_menu.add_command(label="len(data)", command=readoutDataCommand)
third_menu.add_command(label="Data Updated Flag", command=readoutDataUpdatedCommand)
menubar.add_cascade(label="Readouts", menu=third_menu)
root.config(menu=menubar)
root.mainloop()
