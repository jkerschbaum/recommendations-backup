Metrics

Lines of Code:
3805

Lines of Code without comments:
2176

Lines of Code without comments and tests:
1412

Lines of Code (tests only):
764

Number of tests:
104

CI/CD pipeline:
248 runs

Test coverage:
ca. 90%

#Test / #Code:
ca. 36%

PEP8 warnings:
4 (from ProbLog model string, not fixable because the warnings belongs to the model string)