import unittest
import API
from src.data.atomic_actions import clear_data


class MyTestCase(unittest.TestCase):

    def tearDown(self) -> None:
        clear_data()

    def test_system_00(self):
        # No data
        API.initialise()
        result = API.get_recs()
        self.assertEqual(type([]), type(result))
        self.assertEqual(0, len(result))

    def test_system_01(self):
        # DSL-string only
        API.initialise()
        dsl_string = "trim(left).replace(arg1, extractOne())"
        API.new_dsl_string(dsl_string)
        result = API.get_recs()
        self.assertEqual(type([]), type(result))
        self.assertEqual(type(""), type(result[0]))
        self.assertEqual(5, len(result))

    def test_system_02(self):
        # Metadata only
        API.initialise()
        dc = API.empty_dataccarriage()
        dc.selected_single_column = True
        API.new_metadata(dc)
        result = API.get_recs()
        self.assertEqual(type([]), type(result))
        self.assertEqual(type(""), type(result[0]))
        self.assertEqual(5, len(result))

    def test_system_03(self):
        # Both DSL-string and Metadata
        API.initialise()
        dsl_string = "trim(left).replace(arg1, extractOne())"
        API.new_dsl_string(dsl_string)
        dc = API.empty_dataccarriage()
        dc.created_column = True
        dc.selected_single_cell = True
        API.new_metadata(dc)
        result = API.get_recs()
        self.assertEqual(type([]), type(result))
        self.assertEqual(type(""), type(result[0]))
        self.assertEqual(5, len(result))

    def test_system_04(self):
        # Multiple DSL-string only
        API.initialise()
        dsl_string = "trim(left).replace(arg1, extractOne())"
        API.new_dsl_string(dsl_string)
        dsl_string = "replace(arg1, arg2)"
        API.new_dsl_string(dsl_string)
        dsl_string = "replace(arg1, toUpper(extractOne()))"
        API.new_dsl_string(dsl_string)
        result = API.get_recs()
        self.assertEqual(type([]), type(result))
        self.assertEqual(type(""), type(result[0]))
        self.assertEqual(5, len(result))


if __name__ == '__main__':
    unittest.main()
