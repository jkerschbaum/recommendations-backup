import unittest
from src.ml.freq.frequency import initialize, get_frequency_recs
import random


class TestCases(unittest.TestCase):

    def test_initialize(self):
        result = initialize(10, 10)
        self.assertTrue(result)

    def test_get_frequency_recs(self):
        initialize(6, 4)
        data = []
        for i in range(16):
            data.append(random.randint(0, 5))
        result = get_frequency_recs(data)
        self.assertEqual(type(result), type(list()))
        self.assertEqual(type(result[0]), type(0))


if __name__ == '__main__':
    unittest.main()
