import unittest
import random

from src.ml.trans.transmissions import initialize, build_evidence, extract,\
    get_transmission_recs


class MyTestCase(unittest.TestCase):

    def test_initialize(self):
        result = initialize(2)
        self.assertTrue(result)

    def test_build_evidence(self):
        initialize(5)
        data = []
        for i in range(16):
            data.append(random.randint(0, 5))
        evidence = build_evidence(data)
        self.assertEqual(type(evidence), type(list()))
        self.assertEqual(evidence[0][0][2], 0)

    def test_extract(self):
        initialize(2)
        data = [-1, -1, -1, -1, -1, 0, -1, -1, -1]
        result = extract(1, 2, data)
        self.assertEqual(result, 0)

    def test_get_transmission_recs(self):
        initialize(5)
        data = []
        for i in range(16):
            data.append(random.randint(0, 5))
        result = get_transmission_recs(data)
        self.assertEqual(type(result), type(list()))
        self.assertEqual(type(result[0]), type(0))


if __name__ == '__main__':
    unittest.main()
