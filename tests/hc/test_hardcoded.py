import unittest
from src.hc.hardcoded import *
from src.data.atomic_actions import AtomicActions


class TestCases(unittest.TestCase):
    def test_left_pad(self):
        test_string = ""
        self.assertEqual(left_pad(test_string), "0")
        test_string = "xpsk&_"
        self.assertEqual(left_pad(test_string), "0xpsk&_")

    def test_initialize(self):
        self.assertTrue(init_hcr())

    def test_update_dc(self):
        dc1 = DataCarriage()
        dc2 = DataCarriage()
        dc1.created_column = True
        dc1.selected_single_column = True
        dc2.selected_single_cell = True
        dc2.created_column = True
        res = update_dc(dc1, dc2)
        self.assertTrue(res.selected_single_column)
        self.assertTrue(res.selected_single_cell)
        self.assertTrue(res.created_column)

    def test_get_recs_from_carriage(self):
        init_hcr()
        dc = DataCarriage()
        dc.selected_single_column = True
        recs = get_recs_from_carriage(dc)
        self.assertEqual([[AtomicActions.action_toUpper]], recs)


if __name__ == '__main__':
    unittest.main()
