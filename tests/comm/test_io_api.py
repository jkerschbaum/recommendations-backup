import unittest

import API
from src.comm.io_api import rec_new_metadata, rec_new_data,\
    rec_empty_datacarriage, rec_init, get_current_metadata, get_data_updated,\
    start_auto_updater, end_auto_updater, rec_get_recs
from src.hc.hardcoded import DataCarriage
from src.ml.met_mod.meta import EXAMPLES

import random
import time


class TestCases(unittest.TestCase):

    def test_empty_dc(self):
        test_dc = rec_empty_datacarriage()
        original_dc = DataCarriage()
        self.assertEqual(type(test_dc), type(original_dc))
        attributes = [attr for attr in dir(test_dc)
                      if not callable(getattr(test_dc, attr))
                      and not attr.startswith('__')]
        for i in range(len(attributes)):
            self.assertEqual(getattr(test_dc, attributes[i]),
                             getattr(original_dc, attributes[i]))

    def test_new_metadata_0(self):
        dc = DataCarriage()
        rec_new_metadata(dc)
        self.assertEqual(type(dc), type(get_current_metadata()))
        attributes = [attr for attr in dir(dc) if
                      not callable(getattr(dc, attr))
                      and not attr.startswith('__')]
        for i in range(len(attributes)):
            self.assertEqual(getattr(dc, attributes[i]),
                             getattr(get_current_metadata(), attributes[i]))
        self.assertTrue(get_data_updated())

    def test_new_metadata_1(self):
        dc = DataCarriage()
        # Sets 10 random conditions to True
        attributes = [attr for attr in dir(dc)
                      if not callable(getattr(dc, attr))
                      and not attr.startswith('__')]
        for i in range(10):
            setattr(dc, attributes[random.randint(0, len(attributes)-1)], True)
        rec_new_metadata(dc)
        print(dc.created_column == get_current_metadata().created_column)
        self.assertEqual(type(dc), type(get_current_metadata()))
        for i in range(len(attributes)):
            print(attributes[i], getattr(dc, attributes[i]),
                  getattr(get_current_metadata(), attributes[i]))
            self.assertEqual(getattr(dc, attributes[i]),
                             getattr(get_current_metadata(), attributes[i]))
        self.assertTrue(get_data_updated())

    def test_new_data_0(self):
        orig_len = len(EXAMPLES)
        dsl_string = "remove().replaceAll()"
        rec_new_data(dsl_string)
        self.assertTrue(get_data_updated())
        self.assertEqual(len(EXAMPLES), orig_len + 1)

    def test_new_data_1(self):
        orig_len = len(EXAMPLES)
        dsl_string = "replace()"
        rec_new_data(dsl_string)
        self.assertTrue(get_data_updated())
        self.assertEqual(len(EXAMPLES), orig_len + 1)

    def test_rec_init_0(self):
        flag = rec_init()
        self.assertTrue(flag)

    def test_rec_init_1(self):
        flag = rec_init(random.randint(0, 10))
        self.assertTrue(flag)

    def test_get_recs(self):
        dc = DataCarriage()
        # Sets 3 random conditions to True
        attributes = [attr for attr in dir(dc)
                      if not callable(getattr(dc, attr))
                      and not attr.startswith('__')]
        print(len(attributes))
        for i in range(3):
            setattr(dc, attributes[random.randint(0, len(attributes) - 1)],
                    True)
        rec_new_metadata(dc)

        API.initialise()
        dsl_string = "replace(arg1, extractOne())"
        rec_new_data(dsl_string)
        dsl_string = "trim(left)[]()"
        rec_new_data(dsl_string)
        
        result = rec_get_recs()
        print(result)
        self.assertEqual(type(result), type(list()))
        self.assertEqual(type(result[0]), type(""))

    def test_autoupdater_0(self):
        target = []
        thread_id = start_auto_updater(1, target)
        self.assertEqual(type(thread_id), type(int()))
        end_auto_updater(thread_id)

    def test_autoupdater_1(self):
        # Note: Functionality is pre-initialised from earlier tests.
        # This is suboptimal, but we don't have a way to un-initialise stuff
        target = []
        thread_id = start_auto_updater(1, target)
        print(target)
        self.assertTrue((len(target) == 5))
        end_auto_updater(thread_id)

    def test_autoupdater_2(self):
        rec_init(2)
        target = []
        thread_id = start_auto_updater(1, target)
        time.sleep(120)
        print(target)
        self.assertTrue(len(target) == 5)
        self.assertEqual(type(target[0]), type(""))
        end_auto_updater(thread_id)


if __name__ == '__main__':
    unittest.main()
