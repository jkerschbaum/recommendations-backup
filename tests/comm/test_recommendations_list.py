import unittest
import pytest

import src.data.atomic_actions
# from src.data.atomic_actions import clear_data
from src.comm.recommendations_list import *


class TestCases(unittest.TestCase):

    def test_one_d_list_to_list_of_lists_0(self):
        self.assertEqual([[1], [2], [3]],
                         one_d_list_to_list_of_lists([1, 2, 3]))

    def test_one_d_list_to_list_of_lists_1(self):
        self.assertEqual([[]], one_d_list_to_list_of_lists([]))

    def test_int_to_atomic_actions_0(self):
        recs = [[1], [5], [-1]]
        self.assertEqual([[AtomicActions.action_triml],
                          [AtomicActions.action_remove],
                          [AtomicActions.action_none]],
                         int_to_atomic_actions(recs))

    def test_int_to_atomic_actions_1(self):
        recs = [[15]]
        with pytest.raises(ValueError) as expected_error:
            int_to_atomic_actions(recs)
        exception_raised = expected_error.value
        self.assertEqual("15 is not a valid AtomicActions",
                         str(exception_raised))

    def test_get_index_of_random_recs_0(self):
        random.seed(42)
        how_many = 2
        amount_possible = 5
        self.assertEqual([0, 2], get_index_of_random_recs(how_many,
                                                          amount_possible))

    def test_get_index_of_random_recs_1(self):
        random.seed(42)
        how_many = 9
        amount_possible = 1
        self.assertEqual([-1, -1, -1, -1, -1, -1, -1, -1, -1],
                         get_index_of_random_recs(how_many, amount_possible))

    def test_remove_duplicates_0(self):
        random.seed(42)
        list1 = [random.randint(-1, 100) for _ in range(4)]
        list2 = [random.randint(-1, 100) for _ in range(10)]
        self.assertEqual(([80, 13, 2], [34, 30, 27, 16, 93, 12, 85, 68, 10]),
                         remove_duplicates(list1, list2))

    def test_remove_duplicates_1(self):
        random.seed(42)
        list1 = [1, 4]
        list2 = [1, 4]
        self.assertEqual(([], [1, 4]), remove_duplicates(list1, list2))

    def test_distribution(self):
        random.seed(42)
        usage_indicator = 120
        assert (distribution(usage_indicator))

    def test_enough_recs_available_0(self):
        hc_recs = []
        l_recs = []
        amount_hardcoded = 0
        amount_possible_hc = 0
        amount_learned = 0
        amount_possible_l = 0
        percent_learned = 0
        self.assertEqual([], enough_recs_available(hc_recs, amount_hardcoded,
                                                   amount_possible_hc,
                                                   amount_learned,
                                                   amount_possible_l, l_recs,
                                                   percent_learned))

    def test_enough_recs_available_1(self):
        random.seed(42)
        hc_recs = [[AtomicActions.action_remove],
                   [AtomicActions.action_extractOne]]
        l_recs = []
        amount_hardcoded = 2
        amount_possible_hc = len(hc_recs)
        amount_learned = 0
        amount_possible_l = len(l_recs)
        percent_learned = 0.2
        self.assertEqual([[AtomicActions.action_remove],
                          [AtomicActions.action_extractOne]],
                         enough_recs_available(hc_recs, amount_hardcoded,
                                               amount_possible_hc,
                                               amount_learned,
                                               amount_possible_l, l_recs,
                                               percent_learned))

    def test_enough_recs_available_2(self):
        random.seed(42)
        hc_recs = [[AtomicActions.action_remove],
                   [AtomicActions.action_extractOne]]
        l_recs = [[AtomicActions.action_replace], [AtomicActions.action_triml],
                  [AtomicActions.action_elementAccess]]
        amount_hardcoded = 2
        amount_possible_hc = len(hc_recs)
        amount_learned = 2
        amount_possible_l = len(l_recs)
        percent_learned = 0.4
        self.assertEqual([[AtomicActions.action_remove],
                          [AtomicActions.action_extractOne],
                          [AtomicActions.action_elementAccess],
                          [AtomicActions.action_replace]],
                         enough_recs_available(hc_recs, amount_hardcoded,
                                               amount_possible_hc,
                                               amount_learned,
                                               amount_possible_l, l_recs,
                                               percent_learned))

    def test_enough_recs_available_3(self):
        random.seed(42)
        hc_recs = [[AtomicActions.action_remove],
                   [AtomicActions.action_extractOne]]
        l_recs = [[AtomicActions.action_replace], [AtomicActions.action_triml],
                  [AtomicActions.action_elementAccess]]
        amount_hardcoded = 2
        amount_possible_hc = len(hc_recs)
        amount_learned = 2
        amount_possible_l = len(l_recs)
        percent_learned = 0.6
        self.assertEqual([[AtomicActions.action_elementAccess],
                          [AtomicActions.action_replace],
                          [AtomicActions.action_remove],
                          [AtomicActions.action_extractOne]],
                         enough_recs_available(hc_recs, amount_hardcoded,
                                               amount_possible_hc,
                                               amount_learned,
                                               amount_possible_l, l_recs,
                                               percent_learned))

    def test_not_enough_combined(self):
        random.seed(42)
        hc_recs = [[AtomicActions.action_extractOne]]
        l_recs = [[AtomicActions.action_replace], [AtomicActions.action_triml],
                  [AtomicActions.action_elementAccess]]
        amount_hardcoded = 2
        amount_possible_hc = len(hc_recs)
        amount_learned = 3
        amount_possible_l = len(l_recs)
        percent_learned = 0.4
        self.assertEqual([[AtomicActions.action_extractOne],
                          [AtomicActions.action_replace],
                          [AtomicActions.action_triml],
                          [AtomicActions.action_elementAccess],
                          [AtomicActions.action_none]],
                         not_enough_combined(hc_recs, amount_hardcoded,
                                             amount_possible_hc,
                                             amount_learned,
                                             amount_possible_l, l_recs,
                                             percent_learned))

    def test_not_enough_learned_0(self):
        random.seed(42)
        hc_recs = [[AtomicActions.action_replace],
                   [AtomicActions.action_triml],
                   [AtomicActions.action_elementAccess]]
        l_recs = [[AtomicActions.action_remove]]
        amount_hardcoded = 2
        amount_possible_hc = len(hc_recs)
        amount_learned = 2
        amount_possible_l = len(l_recs)
        percent_learned = 0.8
        self.assertEqual([[AtomicActions.action_remove],
                          [AtomicActions.action_replace],
                          [AtomicActions.action_triml],
                          [AtomicActions.action_elementAccess]],
                         not_enough_combined(hc_recs, amount_hardcoded,
                                             amount_possible_hc,
                                             amount_learned,
                                             amount_possible_l, l_recs,
                                             percent_learned))

    def test_not_enough_learned_1(self):
        random.seed(42)
        hc_recs = [[AtomicActions.action_replace],
                   [AtomicActions.action_triml],
                   [AtomicActions.action_elementAccess]]
        l_recs = [[AtomicActions.action_remove]]
        amount_hardcoded = 2
        amount_possible_hc = len(hc_recs)
        amount_learned = 2
        amount_possible_l = len(l_recs)
        percent_learned = 0.2
        self.assertEqual([[AtomicActions.action_replace],
                          [AtomicActions.action_triml],
                          [AtomicActions.action_elementAccess],
                          [AtomicActions.action_remove]],
                         not_enough_combined(hc_recs, amount_hardcoded,
                                             amount_possible_hc,
                                             amount_learned,
                                             amount_possible_l, l_recs,
                                             percent_learned))

    def test_not_enough_hc_0(self):
        random.seed(42)
        l_recs = [[AtomicActions.action_replace], [AtomicActions.action_triml],
                  [AtomicActions.action_elementAccess]]
        hc_recs = [[AtomicActions.action_remove]]
        amount_hardcoded = 2
        amount_possible_hc = len(hc_recs)
        amount_learned = 2
        amount_possible_l = len(l_recs)
        percent_learned = 0.3
        self.assertEqual([[AtomicActions.action_remove],
                          [AtomicActions.action_replace],
                          [AtomicActions.action_triml],
                          [AtomicActions.action_elementAccess]],
                         not_enough_combined(hc_recs, amount_hardcoded,
                                             amount_possible_hc,
                                             amount_learned,
                                             amount_possible_l, l_recs,
                                             percent_learned))

    def test_not_enough_hc_1(self):
        random.seed(42)
        l_recs = [[AtomicActions.action_replace], [AtomicActions.action_triml],
                  [AtomicActions.action_elementAccess]]
        hc_recs = [[AtomicActions.action_remove]]
        amount_hardcoded = 2
        amount_possible_hc = len(hc_recs)
        amount_learned = 2
        amount_possible_l = len(l_recs)
        percent_learned = 0.7
        self.assertEqual([[AtomicActions.action_replace],
                          [AtomicActions.action_triml],
                          [AtomicActions.action_elementAccess],
                          [AtomicActions.action_remove]],
                         not_enough_combined(hc_recs, amount_hardcoded,
                                             amount_possible_hc,
                                             amount_learned,
                                             amount_possible_l, l_recs,
                                             percent_learned))

    def test_get_recs_from_hc_and_l_recs_0(self):
        random.seed(42)
        hc_recs = [[AtomicActions.action_remove, AtomicActions.action_replace],
                   [AtomicActions.action_elementAccess],
                   [AtomicActions.action_padStringValue]]
        l_recs = [1, 5, 7, 2, 12]
        num_of_recs = 5
        self.assertEqual([[AtomicActions.action_remove,
                           AtomicActions.action_replace],
                          [AtomicActions.action_elementAccess],
                          [AtomicActions.action_padStringValue],
                          [AtomicActions.action_triml],
                          [AtomicActions.action_toUpper]],
                         get_recs_from_hc_and_l_recs(hc_recs, l_recs,
                                                     num_of_recs))

    def test_get_recs_from_hc_and_l_recs_1(self):
        random.seed(42)
        # Set length of data to check if more learned recs are present with
        # rising len(data).
        src.data.atomic_actions.data.extend([1] * 200)
        hc_recs = [[AtomicActions.action_remove, AtomicActions.action_replace],
                   [AtomicActions.action_elementAccess],
                   [AtomicActions.action_padStringValue]]
        l_recs = [1, 5, 7, 2, 12]
        num_of_recs = 5
        self.assertEqual([[AtomicActions.action_triml],
                          [AtomicActions.action_toUpper],
                          [AtomicActions.action_remove],
                          [AtomicActions.action_trimr],
                          [AtomicActions.action_remove,
                           AtomicActions.action_replace]],
                         get_recs_from_hc_and_l_recs(hc_recs, l_recs,
                                                     num_of_recs))
        # Empty list again for other test not to fail.
        src.data.atomic_actions.clear_data()


if __name__ == '__main__':
    unittest.main()
