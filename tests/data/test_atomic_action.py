import unittest
from src.data.atomic_actions import *


class TestCases(unittest.TestCase):

    def test_last_actions(self):
        flag = True
        data = [AtomicActions(-1), AtomicActions(2), AtomicActions(3),
                [AtomicActions(3), AtomicActions(4)]]
        for i in range(len(data)):
            if not isinstance(data[i], AtomicActions)\
                    and not isinstance(data[i], list):
                flag = False
        self.assertTrue(flag)

    def test_dummy_data(self):
        generate_dummy_data(MAX_ACTION)
        flag = True
        for i in range(len(data)):
            if not isinstance(data[i], AtomicActions):
                flag = False
        self.assertTrue(flag)

    def test_clear_data(self):
        clear_data()
        self.assertEqual(data, [])

    def test_action_performed_0(self):
        clear_data()
        action = AtomicActions.action_replace
        action_performed(action)
        self.assertEqual(data, [AtomicActions.action_replace])

    def test_action_performed_1(self):
        clear_data()
        first = [AtomicActions.action_elementAccess]
        other = [AtomicActions.action_none] * 299
        data.extend(first)
        data.extend(other)
        self.assertEqual(data[0], first[0])
        action_performed(AtomicActions.action_remove)
        print(data)
        self.assertEqual(data[0], AtomicActions.action_none)


if __name__ == '__main__':
    unittest.main()
