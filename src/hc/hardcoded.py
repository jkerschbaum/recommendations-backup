import src.data.atomic_actions as atomic_actions


# Note: Any change to DataCarriage and/or rec_dict has consequences for
# nearly every other function in this file. Handle this appropriately
# immediately, or stuff breaks

# Total number of Conditions in DataCarriage
NUM_COND = 0

# Total number of returnable recs
NUM_REC = 0

# Associates the keys generated from the DataCarriages with a list of indices
# for the rec_dict
ass_dict = {}

# Contains recs
rec_dict = []


# Convenience-class for data exchange with external sources
# Sources can generate the empty object and then set selected conditions to
# True
class DataCarriage:
    """
        Name:

            DataCarriage

        Behaviour:

            Documents Metadata that may be relevant for the predictions of the
            users next intended step

        Usage:

            1. Generate empty object (Available to external users through
               io_api.rec_empty_datacarriage())
            2. For every Variable in the "Conditionals" section:
                2.1 Check if the described condition is true
                2.2 If so, set it to True
            3. Return the thus filled object to the pipeline (Available to
               external users through io_api.rec_new_metadata(DataCarriage)

        Methods:

            constructor: Initialises all "Conditionals"-Variables to False

        Variables:

            Conditionals: (used as boolean flags to indicate if certain
                           patterns are true at the moment.
                           Conditions that don't last for a longer time
                           always reference if this happened since the last
                           time a DataCarriage was returned to the pipeline.)

                self.created_column:            Did the user create a new
                                                column directly?
                self.selected_single_cell:      Did the user select a single
                                                cell?
                self.selected_multiple_cells:   Did the user select multiple
                                                cells at once?
                self.selected_entire_row:       Did the user select an entire
                                                row at once?
                self.selected_single_column:    Did the user select a single
                                                column?
                self.selected_multiple_columns: Did the user select multiple
                                                columns at once?

    """

    def __init__(self):
        # Define several boolean conditionals and initialise them with False
        self.created_column = False
        self.selected_single_cell = False
        self.selected_multiple_cells = False
        self.selected_entire_row = False
        self.selected_single_column = False
        self.selected_multiple_columns = False


# Represents individual recommendation, as well as the atomics representing
# the DSL-Code needed to execute them as well as the initialize-function
# that grabs necessary data to concretize from groups to specific
class HRec:
    """
        Name:

            HRec

        Behaviour:

            Represents generalised recommendations for the hardcoded system.
            Holds AtomicAction representation and initialize() function that
            gets more data if necessary

        Usage:

            No direct usage. All used HRec Objects are generated upon
            initialisation, and form part of the process
            abstracted by the get_recs_from_carriage(DataCarriage) function.

        Methods:

            constructor: empty

            initialize(self): Dummy function, may be replaced upon
                              initialisation with a specialised function
                              that gets necessary Data to turn the generalised
                              recommendation into a specific one

        Variables:

            atomics: Holds the AtomicAction representation of the
                     recommendation
    """

    # Atomic Actions representing DSL-Code to recommend
    atomics = []

    def initialize(self):
        # Get necessary data
        # Dummy Func, overwritten on initialisation if necessary
        return


# Transmits the Conditions which have been set to True in the DataCarriage to
# the ass_dict-key
def get_key_from_carriage(carr):
    """
            Generates key for association dictionary from DataCarriage carr.
            Generate unique key for each unique set of True Conditions.

            Keyword arguments:
                carr: DataCarriage to generate key from

            Return Values:
                key: Key for association dictionary

            Side effects:
                -

            Possible Exception:
                -

            Restrictions:
                -
    """
    data = []
    key = 0
    # Add for new conditions here
    data.append(carr.created_column)
    data.append(carr.selected_single_cell)
    data.append(carr.selected_multiple_cells)
    data.append(carr.selected_entire_row)
    data.append(carr.selected_single_column)
    data.append(carr.selected_multiple_columns)

    for i in range(len(data)):
        if data[i]:
            key = key | (2 ** i)
    return key


def left_pad(string):
    """
        Left-pads string with 0, i.e. "x" -> "0x"
        Used to ensure leading zeroes aren't lost on conversion from decimal
        to binary

        Keyword arguments:
            string: String to pad

        Return Values:
            <anonymous>: padded string

        Side effects:
            -

        Possible Exception:
            -

        Restrictions:
            -
    """
    return '0' + string


def get_recs_from_dict(key):
    """
            Gets recommendation objects from the dictionaries and initialize
            them

            Keyword arguments:
                key: The key for the association dict (non-optional)

            Return Values:
                ret_recs: List of initialised recommendation Objects

            Side effects:
                -

            Possible Exception:
                -

            Restrictions:
                -
    """
    rec_string = "{0:b}".format(key)
    while len(rec_string) < NUM_REC:
        rec_string = left_pad(rec_string)
    ret_recs = []
    for i in range(len(rec_string)):
        if rec_string[i] == '1':
            ret_recs.append(rec_dict[i])
    for i in ret_recs:
        i.initialize()

    # Return atomics of HRec() objects, not HRec() objects themself
    for i in range(len(ret_recs)):
        ret_recs[i] = ret_recs[i].atomics
    return ret_recs


def initialise_ass_dict():
    """
            Initialises the dictionary that associates the keys generated from
            the DataCarriages with the lists of recommendations

            Keyword arguments:
                -

            Return Values:
                True: Successful
                False: Error occurred

            Side effects:
                -

            Possible Exception:
                -

            Restrictions:
                -
    """
    global ass_dict
    try:
        # NOTE: Values / rec_dict keys for lower indexes will be higher, due
        # to the way get_recs_from_dict works
        # NOTE: Placeholders like the following should be set for every
        # possible, but not assigned value in order to avoid
        # KeyErrors
        # At time of writing, there are 6 Conditions in the DataCarriage, so
        # (2 ** 6) possible values, starting with 0b000
        ass_dict[63] = 63  # 111111  rec: no_rec
        ass_dict[62] = 62  # 111110  rec: no_rec
        ass_dict[61] = 61  # 111101  rec: no_rec
        ass_dict[60] = 60  # 111100  rec: no_rec
        ass_dict[59] = 59  # 111011  rec: no_rec
        ass_dict[58] = 58  # 111010  rec: no_rec
        ass_dict[57] = 57  # 111001  rec: no_rec
        ass_dict[56] = 56  # 111000  rec: no_rec
        ass_dict[55] = 55  # 110111  rec: lower_3
        ass_dict[54] = 54  # 110110  rec: upper_3
        ass_dict[53] = 53  # 110101  rec: trim_left_3
        ass_dict[52] = 52  # 110100  rec: trim_right_3
        ass_dict[51] = 51  # 110011  rec: lower_3
        ass_dict[50] = 50  # 110010  rec: upper_3
        ass_dict[49] = 49  # 110001  rec: trim_left_3
        ass_dict[48] = 48  # 110000  rec: trim_right_3
        ass_dict[47] = 47  # 101111  rec: upper_3
        ass_dict[46] = 46  # 101110  rec: lower_3
        ass_dict[45] = 45  # 101101  rec: upper_3
        ass_dict[44] = 44  # 101100  rec: lower_3
        ass_dict[43] = 43  # 101011  rec: upper_3
        ass_dict[42] = 42  # 101010  rec: remove_element_3
        ass_dict[41] = 41  # 101001  rec: lower_3
        ass_dict[40] = 40  # 101000  rec: upper_3
        ass_dict[39] = 39  # 100111  rec: trim_right_3
        ass_dict[38] = 38  # 100110  rec: trim_left_3
        ass_dict[37] = 37  # 100101  rec: lower_3
        ass_dict[36] = 36  # 100100  rec: upper_3
        ass_dict[35] = 35  # 100011  rec: remove_element_0
        ass_dict[34] = 34  # 100010  rec: remove_element_0
        ass_dict[33] = 33  # 100001  rec: lower_3
        ass_dict[32] = 32  # 100000  rec: upper_3
        ass_dict[31] = 31  # 011111  rec: no_rec
        ass_dict[30] = 30  # 011110  rec: no_rec
        ass_dict[29] = 29  # 011101  rec: no_rec
        ass_dict[28] = 28  # 011100  rec: no_rec
        ass_dict[27] = 27  # 011011  rec: no_rec
        ass_dict[26] = 26  # 011010  rec: no_rec
        ass_dict[25] = 25  # 011001  rec: no_rec
        ass_dict[24] = 24  # 011000  rec: no_rec
        ass_dict[23] = 23  # 010111  rec: no_rec
        ass_dict[22] = 22  # 010110  rec: no_rec
        ass_dict[21] = 21  # 010101  rec: no_rec
        ass_dict[20] = 20  # 010100  rec: trim_both_sides_2
        ass_dict[19] = 19  # 010011  rec: upper_0
        ass_dict[18] = 18  # 010010  rec: lower_0
        ass_dict[17] = 17  # 010001  rec: trim_left_2
        ass_dict[16] = 16  # 010000  rec: trim_right_2
        ass_dict[15] = 15  # 001111  rec: no_rec
        ass_dict[14] = 14  # 001110  rec: no_rec
        ass_dict[13] = 13  # 001101  rec: no_rec
        ass_dict[12] = 12  # 001100  rec: no_rec
        ass_dict[11] = 11  # 001011  rec: lower_1
        ass_dict[10] = 10  # 001010  rec: upper_1
        ass_dict[9] = 9    # 001001  rec: upper_1
        ass_dict[8] = 8    # 001000  rec: lower_1
        ass_dict[7] = 7    # 000111  rec: trim_both_sides_1
        ass_dict[6] = 6    # 000110  rec: trim_right_1
        ass_dict[5] = 5    # 000101  rec: lower_1
        ass_dict[4] = 4    # 000100  rec: upper_1
        ass_dict[3] = 3    # 000011  rec: lower_0
        ass_dict[2] = 2    # 000010, rec: lower_0
        ass_dict[1] = 1    # 000001, rec: no_rec
        ass_dict[0] = 0    # 000000, rec: no_rec
        return True
    except Exception:
        return False


def set_initialize_func(rec, init_func):
    """
        Sets initialize function for given rec Object
        Used to turn a generalised recommendation into a specialised one

        Keyword arguments:
            rec: The HRec Object for which the function is to be set
                 (non-optional)
            init_func: The function to be set as init_func (non-optional)

        Return Values:
            rec: HRec Object with the initialisation function set

        Side effects:
            -

        Possible Exception:
            Who knows. This is some black magic shit.

        Restrictions:
            -
    """
    rec.initialize = init_func.__get__(rec, HRec)
    return rec


def initialise_rec_dict():
    """
        Initialises the dictionary that holds the generalised recommendations

        Keyword arguments:
            -

        Return Values:
        True: Successful
        False: Error occurred

        Side effects:
            -

        Possible Exception:
            -

        Restrictions:
            -
    """
    global NUM_REC, rec_dict
    try:
        trim_both_sides_0 = HRec()  # for selected_single_cell
        trim_both_sides_1 = HRec()  # for selected_multiple_cells
        trim_both_sides_2 = HRec()  # for selected_single_columns
        trim_both_sides_3 = HRec()  # for selected_multiple_columns
        trim_left_0 = HRec()  # for selected_single_cell
        trim_left_1 = HRec()  # for selected_multiple_cells
        trim_left_2 = HRec()  # for selected_single_column
        trim_left_3 = HRec()  # for selected_multiple_column
        trim_right_0 = HRec()  # for selected_single_cell
        trim_right_1 = HRec()  # for selected_multiple_cells
        trim_right_2 = HRec()  # for selected_single_column
        trim_right_3 = HRec()  # for selected_multiple_columns
        remove_element_0 = HRec()  # for selected_single_cell
        remove_element_1 = HRec()  # for selected_multiple_cells
        remove_element_2 = HRec()  # for selected_single_column
        remove_element_3 = HRec()  # for selected_multiple_columns
        lower_0 = HRec()  # for selected_single_cell
        lower_1 = HRec()  # for selected_multiple_cells
        lower_2 = HRec()  # for selected_single_column
        lower_3 = HRec()  # for selected_multiple_columns
        upper_0 = HRec()  # for selected_single_cell
        upper_1 = HRec()  # for selected_multiple_cells
        upper_2 = HRec()  # for selected_single_column
        upper_3 = HRec()  # for selected_multiple_columns
        no_rec = HRec()  # no action

        trim_both_sides_0.atomics = [atomic_actions.AtomicActions(0)]
        trim_both_sides_1.atomics = [atomic_actions.AtomicActions(0)]
        trim_both_sides_2.atomics = [atomic_actions.AtomicActions(0)]
        trim_both_sides_3.atomics = [atomic_actions.AtomicActions(0)]
        trim_left_0.atomics = [atomic_actions.AtomicActions(1)]
        trim_left_1.atomics = [atomic_actions.AtomicActions(1)]
        trim_left_2.atomics = [atomic_actions.AtomicActions(1)]
        trim_left_3.atomics = [atomic_actions.AtomicActions(1)]
        trim_right_0.atomics = [atomic_actions.AtomicActions(2)]
        trim_right_1.atomics = [atomic_actions.AtomicActions(2)]
        trim_right_2.atomics = [atomic_actions.AtomicActions(2)]
        trim_right_3.atomics = [atomic_actions.AtomicActions(2)]
        remove_element_0.atomics = [atomic_actions.AtomicActions(5)]
        remove_element_1.atomics = [atomic_actions.AtomicActions(5)]
        remove_element_2.atomics = [atomic_actions.AtomicActions(5)]
        remove_element_3.atomics = [atomic_actions.AtomicActions(5)]
        lower_0.atomics = [atomic_actions.AtomicActions(6)]
        lower_1.atomics = [atomic_actions.AtomicActions(6)]
        lower_2.atomics = [atomic_actions.AtomicActions(6)]
        lower_3.atomics = [atomic_actions.AtomicActions(6)]
        upper_0.atomics = [atomic_actions.AtomicActions(7)]
        upper_1.atomics = [atomic_actions.AtomicActions(7)]
        upper_2.atomics = [atomic_actions.AtomicActions(7)]
        upper_3.atomics = [atomic_actions.AtomicActions(7)]
        no_rec.atomics = [atomic_actions.AtomicActions(-1)]

        # If we had to replace the initialize function, we would do it here
        rec_dict.append(no_rec)
        rec_dict.append(no_rec)
        rec_dict.append(no_rec)
        rec_dict.append(no_rec)
        rec_dict.append(no_rec)
        rec_dict.append(no_rec)
        rec_dict.append(no_rec)
        rec_dict.append(no_rec)
        rec_dict.append(lower_3)
        rec_dict.append(upper_3)
        rec_dict.append(trim_left_3)
        rec_dict.append(trim_right_3)
        rec_dict.append(lower_3)
        rec_dict.append(upper_3)
        rec_dict.append(trim_left_3)
        rec_dict.append(upper_3)
        rec_dict.append(lower_3)
        rec_dict.append(upper_3)
        rec_dict.append(lower_3)
        rec_dict.append(upper_3)
        rec_dict.append(remove_element_3)
        rec_dict.append(lower_3)
        rec_dict.append(upper_3)
        rec_dict.append(trim_right_3)
        rec_dict.append(trim_left_3)
        rec_dict.append(lower_3)
        rec_dict.append(upper_3)
        rec_dict.append(remove_element_0)
        rec_dict.append(remove_element_0)
        rec_dict.append(lower_3)
        rec_dict.append(upper_3)
        rec_dict.append(no_rec)
        rec_dict.append(no_rec)
        rec_dict.append(no_rec)
        rec_dict.append(no_rec)
        rec_dict.append(no_rec)
        rec_dict.append(no_rec)
        rec_dict.append(no_rec)
        rec_dict.append(no_rec)
        rec_dict.append(no_rec)
        rec_dict.append(no_rec)
        rec_dict.append(no_rec)
        rec_dict.append(trim_both_sides_2)
        rec_dict.append(upper_0)
        rec_dict.append(lower_0)
        rec_dict.append(trim_left_2)
        rec_dict.append(trim_right_2)
        rec_dict.append(no_rec)
        rec_dict.append(no_rec)
        rec_dict.append(no_rec)
        rec_dict.append(no_rec)
        rec_dict.append(lower_1)
        rec_dict.append(upper_1)
        rec_dict.append(upper_1)
        rec_dict.append(lower_1)
        rec_dict.append(trim_both_sides_1)
        rec_dict.append(trim_right_1)
        rec_dict.append(lower_1)
        rec_dict.append(upper_1)
        rec_dict.append(lower_0)
        rec_dict.append(lower_0)
        rec_dict.append(no_rec)
        rec_dict.append(no_rec)

        # Init
        NUM_REC = len(rec_dict)
        return True
    except Exception:
        return False


def init_hcr():
    """
        Initialises functionality for hardcoded recommendations

        Keyword arguments:
            -

        Return Values:
            True: Successful
            False: Error occurred

        Side effects:
            -

        Possible Exception:
            -

        Restrictions:
            -
    """
    ass_worked = initialise_ass_dict()
    rec_worked = initialise_rec_dict()
    return not ((not ass_worked) or (not rec_worked))


def get_recs_from_carriage(carr):
    """
        Controller function. Automates process of getting hardcoded recs from
        externally prepared Data Carriage.
        Workflow:
            1. Convert carriage conditionals to association key
            2. Get list of relevant recs by association key
            3. Get generalised recs by the list
            4. Initialise/Concretise recs

        Keyword arguments:
            carr: DataCarriage Object to generate recs from (non-optional)

        Return Values:
            output_recs: Prepared recs in API-required format

        Side effects:
            -

        Possible Exception:
            -

        Restrictions:
            -
    """
    # The conditionals in the DataCarriage are used to create a unique key
    key = get_key_from_carriage(carr)
    # Each key is associated with a certain recommendation
    rec_key = ass_dict[key]
    # Gets the recommendations from the rec_dict and initialises them
    ret_recs = get_recs_from_dict(rec_key)
    return ret_recs


# Create DataCarriage that combines the Condition Values of both inputs
# a + b = 1 unless a = b = 0; 0 + 0 = 0
def update_dc(dc1, dc2):
    """
    Creates unified DataCarriage from arguments. Any Condition that is true in
    either is true in the result.

    Keyword arguments:
        dc1: One of the DataCarriages to be unified (non-optional)
        dc2: The other DataCarriage to be unified (non-optional)

    Return Values:
        ret_dec: Unified DataCarriage with all true conditions from the
        arguments

    Side effects:
        -

    Possible Exception:
        -

    Restrictions:
        -
    """
    ret_dc = DataCarriage()
    members = [attr for attr in dir(ret_dc) if
               not callable(getattr(ret_dc, attr)) and not attr.startswith(
                   '__')]
    for i in range(len(members)):
        if getattr(dc1, members[i]) or getattr(dc2, members[i]):
            setattr(ret_dc, members[i], True)
        else:
            setattr(ret_dc, members[i],
                    False)  # Should always be the case, but just to be certain
    return ret_dc
