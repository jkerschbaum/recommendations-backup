import enum
import random


NO_ACTIONS = 13
MAX_ACTION = 13
MIN_ACTION = 0

data = []
DATA_LIMIT = 300


class AtomicActions(enum.Enum):
    """
        Anonymize DSL-ACTIONS, associated by translator functions
        Note: action_none is a Dummy for initialising last_actions,
        also used for cases where there probably should be an action, but none
        of the specific ones apply
    """
    action_none = -1
    action_trim = 0
    action_triml = 1
    action_trimr = 2
    action_replace = 3
    action_replaceAll = 4
    action_remove = 5
    action_toLower = 6
    action_toUpper = 7
    action_extractOne = 8
    action_extractAll = 9
    action_split = 10
    action_padStringValue = 11
    action_elementAccess = 12


def clear_data():
    """
       Clears data variable. Only for testing purposes.

       Keyword arguments:
           -

       Return Values:
           -

       Side effects:
           -

       Possible Exception:
           -

       Restrictions:
           -
    """
    data.clear()


def action_performed(action):
    """
       Adds new (atomic) action taken by user to data representations

       Keyword arguments:
           action: AtomicActions object representing the action to be added

       Return Values:
           -

       Side effects:
           Oldest action in last_actions is removed
           (required - constant length)

       Possible Exception:
           -

       Restrictions:
           -
    """
    global data
    data.append(action)
    if len(data) > DATA_LIMIT:
        data.pop(0)


# TODO: Generate longer Term Data
def generate_dummy_data(num_actions):
    """
        Generates (theoretically pattern-less) dummy data for various purposes
        Note: The data will not be returned directly, it will be generated for
        the currently in-place
        recommendation system to be used

        Keyword arguments:
            num_actions: The amount of actions to be generated

        Return Values:
            -

        Side effects:
            -

        Possible Exception:
            -

        Restrictions:
            -
    """
    for i in range(num_actions):
        num = random.randint(MIN_ACTION, NO_ACTIONS-2)
        action_performed(AtomicActions(num))
