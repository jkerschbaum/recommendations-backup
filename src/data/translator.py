import re
from src.data.atomic_actions import AtomicActions

FUNCTIONS = ["trim", "trim", "trim", "replace", "replaceAll", "remove",
             # 0        1      2         3            4            5
             "toLower", "toUpper", "extractOne",
             #   6          7           8
             "extractAll", "split", "padStringValue",
             #     9          10           11
             "[]"]  # <- 12 ; trim(), trim(left), trim(right)

UNIDENTIFIED_FUNC = False


def preformat(dsl_string):
    """
                    Handles various pre-translation formatting tasks

                    Keyword arguments:
                        dsl_string: string in DSL-format

                    Return Values:
                        dsl_string: same string, reformatted

                    Side effects:
                        -

                    Possible Exception:
                        -

                    Restrictions:
                        -
        """
    dsl_string = re.sub("\".*\"", "", dsl_string)
    dsl_string = re.sub("\'.*\'", "", dsl_string)
    # Remove all strings, prevents issues with operators in strings
    dsl_string = re.sub("\\[[\\w.^]*]", "[]()", dsl_string)
    # Ensures that the ElementAccess operator can be treated like a function
    dsl_string = dsl_string.replace(",", ", ")
    # Ensure all commas are followed by a space
    dsl_string = dsl_string.replace(".", ". ")
    # Ensures chained functions are detected properly
    dsl_string = dsl_string.replace("(", "( ")
    # Ensures that functions without arguments are detected properly
    dsl_string = dsl_string.replace("[", " [")
    # Ensure that ElementAccess operators are detected properly if used on
    # object resulting from function
    # (i.e. func()[])
    dsl_string = dsl_string.replace(")", " )")
    # Ensures multi-level functions where the top level does not have another
    # argument are detected properly
    dsl_string = remove_non_space_whitespace(dsl_string)
    return dsl_string


def remove_non_space_whitespace(dsl_string):
    """
                    Removes various non-space whitespaces from input

                    Keyword arguments:
                        dsl_string: string in DSL-format

                    Return Values:
                        dsl_string: same string without various non-space
                                    whitespaces

                    Side effects:
                        -

                    Possible Exception:
                        -

                    Restrictions:
                        -
        """
    dsl_string = dsl_string.replace("\n", " ")
    dsl_string = dsl_string.replace("\t", " ")
    dsl_string = dsl_string.replace("\r", " ")
    # TODO: Any other relevant ones?
    return dsl_string


def has_function_name(
        input_string):  # Mainly exists to make main parser easier to read
    """
                    Checks if the token contains a function call beginning

                    Keyword arguments:
                        input_string: string to be checked

                    Return Values:
                        <anonymous>: T/F

                    Side effects:
                        -

                    Possible Exception:
                        -

                    Restrictions:
                        -
        """
    return "(" in input_string


def get_func_end_index(dsl_string_token, func_start_index):
    """
                    Finds the token that contains the end of a given function

                    Keyword arguments:
                        dsl_string_token: list of tokens
                        func_start_index: index of the token where the
                                          beginning of the function is found

                    Return Values:
                        <anonymous>: index of the token where the function ends

                    Side effects:
                        -

                    Possible Exception:
                        -

                    Restrictions:
                        -
    """
    open_counter = 1
    for i in range(func_start_index + 1, len(dsl_string_token)):
        for ch in dsl_string_token[i]:
            if ch == "(":
                open_counter += 1
            if ch == ")":
                open_counter -= 1
        if open_counter == 0:
            # Aka. if all open parentheses have been closed
            return i


def dsl_to_atomics(dsl_string):
    """
                    Translates a string in DSL-format to a list of atomic
                    actions

                    Keyword arguments:
                        dsl_string: string in DSL-format to be translated

                    Return Values:
                        atomics: list of atomic actions

                    Side effects:
                        -

                    Possible Exception:
                        -

                    Restrictions:
                        -
    """
    global UNIDENTIFIED_FUNC
    UNIDENTIFIED_FUNC = False
    try:
        dsl_string = preformat(dsl_string)
        dsl_string_tokens = dsl_string.split()
        # Input formatted and roughly tokenized
        atomics_list = []
        function_list = []
        while True:
            # Find all top-level functions in input tokens
            func_start_index = 0
            function_found = False
            if len(function_list) != 0:
                func_start_index = function_list[-1][1] + 1
            for i in range(func_start_index, len(dsl_string_tokens)):
                if has_function_name(dsl_string_tokens[i]):
                    func_start_index = i
                    function_found = True
                    break
            if function_found:
                func_end_index = get_func_end_index(dsl_string_tokens,
                                                    func_start_index)
                function_list.append((func_start_index, func_end_index))
            else:
                break
        # All functions found
        for func in function_list:
            # Translate inner stuff, this gets executed FIRST, so it gets
            # into the list first
            inner_slice = dsl_string_tokens[func[0] + 1: func[1]]
            inner_string = " ".join(inner_slice)
            return_list = dsl_to_atomics(inner_string)
            for i in return_list:
                atomics_list.append(i)
            # Translate local function
            func_name = ""
            for i in range(len(FUNCTIONS)):
                if FUNCTIONS[i] in dsl_string_tokens[func[0]]:
                    func_name = FUNCTIONS[i]
                    break
            if func_name != "":
                ind = FUNCTIONS.index(func_name)
                if ind == 0:  # Yes, this applies right > left > both upwards
                    # even if the upper level
                    # has a lower indicator. According to correspondence with
                    # DSL team, this only occurs in
                    # case of severe user error, so we'll assume we only ever
                    # get one indicator per call.
                    if "left" in inner_string:
                        ind = 1
                    if "right" in inner_string:
                        ind = 2
                atomics_list.append(AtomicActions(ind))
            else:
                UNIDENTIFIED_FUNC = True
    except Exception:
        atomics_list = [AtomicActions(-1)]
    while AtomicActions(-1) in atomics_list:
        atomics_list.remove(AtomicActions(-1))
        # -1s occur when there is no function in input, or a variety of other
        # issues. But they don't correspond to
        # anything specific. We thus don't want the models to learn them, so
        # we keep them out of the translators output.
    if not UNIDENTIFIED_FUNC:
        # If everything is fine, return the list
        return atomics_list
    else:
        return [AtomicActions(-1)]


# These are more or less arbitrary user hints, as the system cannot currently
# handle arguments, only function calls
placeholder_args = {
    0: "",
    1: "left",
    2: "right",
    3: "target, replacement",
    4: "@{}",
    5: "target",
    6: "",
    7: "",
    8: "target",
    9: "pattern",
    10: "delimiter",
    11: "side, amount, pad_char",
    12: ""
}


def atomics_to_dsl(atomics_list):
    """
                Translates a list of atomic actions to a string in DSL format

                Keyword arguments:
                    atomics_list: list of atomic actions to be translated

                Return Values:
                    dsl_string: string in DSL-format

                Side effects:
                    -

                Possible Exception:
                    -

                Restrictions:
                    -
    """
    # TODO: Ensure "[]" is used properly?
    if not isinstance(atomics_list, list):
        atomics_list = [atomics_list]
    dsl_string_tokens = []
    for action in atomics_list:
        if action.value == -1:
            continue
        dsl_string_tokens.append(FUNCTIONS[action.value])
        if action.value != 12:
            dsl_string_tokens[-1] += "("
            dsl_string_tokens[-1] += placeholder_args.get(action.value,
                                                          "INVALID")
            dsl_string_tokens[-1] += ")"
    dsl_string_tokens = ".".join(dsl_string_tokens)
    dsl_string_tokens = dsl_string_tokens.replace(".[]", "[]")
    return dsl_string_tokens
