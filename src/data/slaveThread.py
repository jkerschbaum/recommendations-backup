import threading


def generate_thread(func, target, *args):
    """
                    Thread factory for multithreading purposes. Accepts varying
                    number of argument.
                    (Currently 0, 1 or 2)

                    Keyword arguments:
                        func: The function for the new thread to execute
                        target: Variable to write the result of the function to
                        *args: Arguments for func (0 - 2 expected)

                    Return Values:
                        -

                    Side effects:
                        Spawns new thread

                    Possible Exception:
                        -

                    Restrictions:
                        Only supports up to 2 arguments
    """

    # Target has to be a list since we're abusing the fact that Python passes
    # variables by assignment to populate target.
    # As such, the relevant functions need to be implemented by the datatype
    # for target.
    # Custom types could fulfill this, but for simplicities sake we're
    # restricting this to inbuilts only, which means
    # that only lists fulfill all the requirements.
    if not isinstance(target, list):
        raise RuntimeError("Target is not a list")

    # If the length isn't 0 at this point, then the target would contain
    # junk data that's not part of the expected.
    # return once the thread terminates
    if len(target) != 0:
        raise RuntimeError("Target has wrong size, len(target) must be"
                           "exactly 0")

    if len(args) == 0:
        return generate_thread_0(func, target)
    elif len(args) == 1:
        return generate_thread_1(func, target, args[0])
    elif len(args) == 2:
        return generate_thread_2(func, target, args[0], args[1])
    else:
        # Could be expanded to include threads for as many arguments as
        # necessary
        raise RuntimeError("No Thread factory available for {} arguments"
                           .format(len(args)))


def generate_thread_0(func, target):
    """
                    Generates thread to execute function with 0 arguments

                    Keyword arguments:
                        func: function to execute
                        target: variable to write result to

                    Return Values:
                        -

                    Side effects:
                        Spawns new thread

                    Possible Exception:
                        -

                    Restrictions:
                        No arguments
        """

    class slaveThread(threading.Thread):
        def __init__(self, func, target):
            threading.Thread.__init__(self)
            self.func = func
            self.target = target

        def run(self):
            ret_list = func()
            for i in range(len(ret_list)):
                self.target.append(0)
                self.target.__setitem__(i, ret_list[i])

    return slaveThread(func, target)


def generate_thread_1(func, target, arg):
    """
                    Generates thread to execute function with 1 argument

                    Keyword arguments:
                        func: function to execute
                        target: variable to write result to
                        arg: argument to func

                    Return Values:
                        -

                    Side effects:
                        Spawns new thread

                    Possible Exception:
                        -

                    Restrictions:
                        Exactly 1 argument
        """

    class slaveThread(threading.Thread):
        def __init__(self, func, target, arg):
            threading.Thread.__init__(self)
            self.func = func
            self.target = target
            self.arg = arg

        def run(self):
            ret_list = func(arg)
            for i in range(len(ret_list)):
                self.target.append(0)
                self.target.__setitem__(i, ret_list[i])

    return slaveThread(func, target, arg)


def generate_thread_2(func, target, arg1, arg2):
    """
                    Generates thread to execute function with 2 arguments

                    Keyword arguments:
                        func: function to execute
                        target: variable to write result to
                        arg1: first argument to func
                        arg2: second argument to func

                    Return Values:
                        -

                    Side effects:
                        Spawns new thread

                    Possible Exception:
                        -

                    Restrictions:
                        Exactly 2 arguments
        """

    class slaveThread(threading.Thread):
        def __init__(self, func, target, arg1, arg2):
            threading.Thread.__init__(self)
            self.func = func
            self.target = target
            self.arg1 = arg1
            self.arg2 = arg2

        def run(self):
            ret_list = func(arg1, arg2)
            for i in range(len(ret_list)):
                self.target.append(0)
                self.target.__setitem__(i, ret_list[i])

    return slaveThread(func, target, arg1, arg2)
