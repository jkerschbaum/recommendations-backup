import src.ml.trans.transmissions as transmissions
import src.ml.freq.frequency as frequency
import src.ml.met_mod.meta as meta
from src.data.slaveThread import generate_thread


def init_learned(max_action, num_of_recs):
    """
            Initialises functionality for learned recommendations

            Keyword arguments:
                -

            Return Values:
                True: Successful
                False: An error occurred at some point

            Side effects:
                -

            Possible Exception:
                -

            Restrictions:
                -
        """
    transmission_worked = transmissions.initialize(max_action, num_of_recs)
    frequency_worked = frequency.initialize(max_action, num_of_recs)
    meta_worked = meta.initialise()
    return (not ((not transmission_worked) or (not frequency_worked)
                 or (not meta_worked)))


def get_learned_recs(data):
    """
            Controller function. Automates process of getting learned recs from
            externally prepared Data.
            Workflow:
                1. Convert Data to integers
                2. Get most likely follow-up actions from
                   transmission-probability model
                3. Get most likely follow-up actions from frequency analysis
                   model
                4. Unify lists through the met_mod-model

            Keyword arguments:
                data: list of atomic actions that describes the actions taken
                      by the user

            Return Values:
                unified_recs: unified list of atomic actions provided by the
                              models, that represent likely next step for the
                              user

            Side effects:
                -

            Possible Exception:
                RuntimeError: Transmission Model not initialised

            Restrictions:
                -
        """
    if len(data) == 0:
        return []
    int_data = []
    for d in data:
        int_data.append(d.value)

    transmission_recs = []
    thread = generate_thread(transmissions.get_transmission_recs,
                             transmission_recs, int_data)
    thread.start()

    frequency_recs = frequency.get_frequency_recs(int_data)

    thread.join()
    unified_recs = meta.unify_recs(transmission_recs, frequency_recs)
    return unified_recs
