import src.ml.freq.freq_bigram as bi
import src.ml.freq.freq_trigram as tri

MAX_ACTION = 0
NUM_OF_RECS = 0


def initialize(max_action, num_of_recs):
    """
                Initialises frequency analysis model and connected
                functionality

                Keyword arguments:
                    max_action: The number of the last action. Assumes
                                0-indexed, so #Actions - 1 (non-optional)
                    num_of_recs: The number of recommendations to be prepared
                                 (default: 5)

                Return Values:
                    True: Successful
                    False: An error occurred at some point

                Side effects:
                    -

                Possible Exception:
                    -

                Restrictions:
                    Only call once
    """
    global MAX_ACTION, NUM_OF_RECS
    try:
        if not isinstance(max_action, int) or not isinstance(num_of_recs, int):
            return False
        MAX_ACTION = max_action
        NUM_OF_RECS = num_of_recs
        return True
    except Exception:
        return False


def get_probs_next_unified(probs_next, probs_next_bi, probs_next_tri):
    """
            Unifies unigram, bigram and trigram probabilities

            Keyword arguments:
                probs_next:     unigram probability list
                probs_next_bi:  bigram probability list
                probs_next_tri: trigram probability list

            Return Values:
                probs_next_unified: unified list of follow-up actions
                                    probabilities

            Side effects:
                -

            Possible Exception:
                -

            Restrictions:
                -
    """
    probs_next_unified = [0] * (MAX_ACTION + 1)
    for i in range(MAX_ACTION + 1):
        probs_next_unified[i] = ((probs_next[i] + probs_next_bi[i] +
                                  probs_next_tri[i]) / 3)
    return probs_next_unified


def get_frequency_recs(int_data):
    """
                Controller Function. Automates process of getting learned recs
                from externally prepared data

                Keyword arguments:
                    int_data: list of atomic actions in integer format that
                              describes the actions taken by the user
                              (non-optional)

                Return Values:
                    frequency_recommendations: List of ints that represent the
                                               possible next user actions as
                                               predicted by the frequency
                                               analysis

                Side effects:
                    -

                Possible Exception:
                    -

                Restrictions:
                    -
    """
    amounts = [0] * (MAX_ACTION + 1)
    probabilities = [0.0] * (MAX_ACTION + 1)

    amounts = count_uni_amounts(int_data, amounts)

    for i in range(MAX_ACTION + 1):
        probabilities[i] = amounts[i] / len(int_data)

    probs_next = next_probability(int_data, probabilities)
    probs_next_bi = bi.get_bigram_recs(int_data, MAX_ACTION)
    probs_next_tri = tri.get_trigram_recs(int_data, MAX_ACTION)
    probs_next_unified = get_probs_next_unified(probs_next, probs_next_bi,
                                                probs_next_tri)

    # Gathers frequencies / occurrence probabilities for any given unigram,
    # bigram and trigram.
    # Calculates the absolute probability for any possible followup action
    # through them
    # Then unifies the three generated probabilities for each possible next
    # action by averaging them

    top = []
    probs_next_unified_sorted = sorted(probs_next_unified)
    probs_next_unified_sorted.reverse()
    for i in range(len(probs_next_unified_sorted)):
        index = probs_next_unified.index(probs_next_unified_sorted[i])
        top.append(index)
        if len(set(top)) == NUM_OF_RECS or (len(set(top)) == MAX_ACTION
                                            and MAX_ACTION > NUM_OF_RECS):
            break
    frequency_recommendations = []
    [frequency_recommendations.append(x) for x in top if x
     not in frequency_recommendations]
    while (len(frequency_recommendations) < NUM_OF_RECS
           and len(frequency_recommendations) < MAX_ACTION):
        frequency_recommendations.append(-1)
    return frequency_recommendations


def count_uni_amounts(int_data, amounts):
    """
            Used to smooth unigram amounts and calculate relevant probabilities

            Keyword arguments:
                int_data: list of atomic actions in integer format that
                          describes the actions taken by the user
                          (non-optional)

            Return Values:
                -

            Side effects:
                Changes globals AMOUNTS and PROBABILITIES

            Possible Exception:
                -

            Restrictions:
                -
    """
    for i in int_data:
        amounts[i] += 1
    # Smoothing, prevents any probability being 0
    for i in range(len(amounts)):
        amounts[i] += 1
    return amounts


def next_probability(int_data, probabilities):
    """
            Creates and returns unigram-followup probabilities

            Keyword arguments:
                int_data: list of atomic actions in integer format that
                          describes the actions taken by the user
                          (non-optional)
                probabilities: Overall unigram occurrence probabilities

            Return Values:
                probs_next: unigram followup probabilities

            Side effects:
                -

            Possible Exception:
                -

            Restrictions:
                -
    """
    probs_next = [0] * (MAX_ACTION + 1)

    seq_prob = 10 ** len(int_data)  # TODO: Find better starting probability
    # NOTE: This step isn't technically necessary at this point, but will be
    # necessary once more than unigram
    # probabilities will be considered
    for i in int_data:
        seq_prob *= probabilities[i]

    for i in range(MAX_ACTION + 1):
        probs_next[i] = seq_prob * probabilities[i]

    return probs_next
