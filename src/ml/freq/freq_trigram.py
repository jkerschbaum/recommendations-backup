MAX_ACTION = 0


def count_tri_amounts(amounts, int_data):
    """
            Counts amounts of specific trigrams in known sequence,
            smooths data

            Keyword arguments:
                amounts: 3D-list to be filled
                int_data: list of atomic actions in integer format that
                          describes the actions taken by the user
                          (non-optional)

            Return Values:
                amounts: 3D-list where amounts[i][j][k] is equal to #times the
                         bigram (i, j, k) appears in the data + 1

            Side effects:
                -

            Possible Exception:
                -

            Restrictions:
                -
    """
    for i in range(len(int_data))[:len(int_data)-2]:
        amounts[int_data[i]][int_data[i+1]][int_data[i+2]] += 1

    for i in range(MAX_ACTION + 1):
        for j in range(MAX_ACTION + 1):
            for k in range(MAX_ACTION + 1):
                amounts[i][j][k] += 1

    return amounts


def get_tri_array():
    """
                Creates 3D list of size (MAX_ACTION x MAX_ACTION x MAX_ACTION)

                Keyword arguments:
                    -

                Return Values:
                    arr: 3d list

                Side effects:
                    -

                Possible Exception:
                    -

                Restrictions:
                    -
        """
    arr = []
    for i in range(MAX_ACTION + 1):
        arr.append([])
        for j in range(MAX_ACTION + 1):
            arr[i].append([])
            for k in range(MAX_ACTION + 1):
                arr[i][j].append(0)
    return arr


def get_tri_seq_prob(probabilities, int_data):
    """
            Calculates sequence probability for known sequence on basis of
            trigram model

            Keyword arguments:
                probabilities: list of occurrence probabilities of specific
                               trigrams
                int_data: list of atomic actions in integer format that
                          describes the actions taken by the user
                          (non-optional)

            Return Values:
                seq_prob: sequence probability for known sequence under
                          trigram model

            Side effects:
                -

            Possible Exception:
                -

            Restrictions:
                -
    """
    seq_prob = 10 ** len(int_data)
    for i in range(len(int_data))[:len(int_data) - 2]:
        seq_prob *= probabilities[int_data[i]][int_data[i+1]][int_data[i+2]]
    return seq_prob


def calculate_tri_probs(probabilities, seq_prob, int_data):
    """
            Calculates trigram follow-up probabilities

            Keyword arguments:
                probabilities: list of calculated occurrence probabilities for
                               each possible next trigram
                seq_prob: probability of known sequence under trigram model
                int_data: list of atomic actions in integer format that
                          describes the actions taken by the user
                          (non-optional)

            Return Values:
                probs_next_tri: list of follow-up probabilities for each
                                possible next action

            Side effects:
                -

            Possible Exception:
                -

            Restrictions:
                -
    """
    last_action = int_data[-1]
    second_to_last_action = int_data[-2]
    probs_next_tri = [0] * (MAX_ACTION + 1)

    for i in range(MAX_ACTION + 1):
        probs_next_tri[i] = (
                seq_prob
                * probabilities[second_to_last_action][last_action][i])

    return probs_next_tri


def get_trigram_recs(int_data, max_action):
    """
        Calculates trigram frequency, returns probabilities for next action
        based on these

        Keyword arguments:
            int_data: list of atomic actions in integer format that describes
                      the actions taken by the user
                      (non-optional)
            max_action: MAX_ACTION

        Return Values:
            probs_next_tri: probabilities for the possible next actions based
                            on trigram probabilities

        Side effects:
            -

        Possible Exception:
            -

        Restrictions:
            -
    """
    global MAX_ACTION
    MAX_ACTION = max_action
    amounts = get_tri_array()
    # Count occurrences of trigrams, increase relevant amount field for each,
    # smooths so that no 0-amount remains
    amounts = count_tri_amounts(amounts, int_data)

    probabilities = get_tri_array()
    for i in range(MAX_ACTION + 1):
        for j in range(MAX_ACTION + 1):
            for k in range(MAX_ACTION + 1):
                probabilities[i][j][k] = (amounts[i][j][k] / (len(int_data)
                                                              - 2))

    seq_prob = get_tri_seq_prob(probabilities, int_data)
    return calculate_tri_probs(probabilities, seq_prob, int_data)
