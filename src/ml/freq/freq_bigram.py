MAX_ACTION = 0


def count_bi_amounts(amounts, int_data):
    """
            Counts amounts of specific bigrams in known sequence, smooths data

            Keyword arguments:
                amounts: 2D-list to be filled
                int_data: list of atomic actions in integer format that
                          describes the actions taken by the user
                          (non-optional)

            Return Values:
                amounts: 2D-list where amounts[i][j] is equal to #times the
                         bigram (i, j) appears in teh data + 1

            Side effects:
                -

            Possible Exception:
                -

            Restrictions:
                -
    """
    for i in range(len(int_data))[:len(int_data)-1]:
        amounts[int_data[i]][int_data[i+1]] += 1

    for i in range(MAX_ACTION + 1):
        for j in range(MAX_ACTION + 1):
            amounts[i][j] += 1

    return amounts


def get_bi_array():
    """
            Creates 2D list of size (MAX_ACTION x MAX_ACTION)

            Keyword arguments:
                -

            Return Values:
                arr: 2d list

            Side effects:
                -

            Possible Exception:
                -

            Restrictions:
                -
    """
    arr = []
    for i in range(MAX_ACTION + 1):
        arr.append([])
        for j in range(MAX_ACTION + 1):
            arr[i].append(0)
    return arr


def get_bi_seq_prob(probabilities, int_data):
    """
            Calculates sequence probability for known sequence on basis of
            bigram model

            Keyword arguments:
                probabilities: list of occurrence probabilities of specific
                               bigrams
                int_data: list of atomic actions in integer format that
                          describes the actions taken by the user
                          (non-optional)

            Return Values:
                seq_prob: sequence probability for known sequence under
                bigram model

            Side effects:
                -

            Possible Exception:
                -

            Restrictions:
                -
    """
    seq_prob = 10 ** len(int_data)
    for i in range(len(int_data))[:len(int_data) - 1]:
        seq_prob *= probabilities[int_data[i]][int_data[i+1]]
    return seq_prob


def calculate_bi_probs(probabilities, seq_prob, int_data):
    """
            Calculates bigram follow-up probabilities

            Keyword arguments:
                probabilities: list of calculated occurrence probabilities for
                               each possible next bigram
                seq_prob: probability of known sequence under bigram model
                int_data: list of atomic actions in integer format that
                          describes the actions taken by the user
                          (non-optional)

            Return Values:
                probs_next_bi: list of follow-up probabilities for each
                               possible next action

            Side effects:
                -

            Possible Exception:
                -

            Restrictions:
                -
    """
    last_action = int_data[-1]
    probs_next_bi = [0] * (MAX_ACTION + 1)

    for i in range(MAX_ACTION + 1):
        probs_next_bi[i] = seq_prob * probabilities[last_action][i]

    return probs_next_bi


def get_bigram_recs(int_data, max_action):
    """
            Calculates bigram frequency, returns probabilities for next action
            based on these

            Keyword arguments:
                int_data: list of atomic actions in integer format that
                          describes the actions taken by the user
                          (non-optional)
                max_action: MAX_ACTION

            Return Values:
                probs_next_bi: probabilities for the possible next actions
                based on bigram probabilities

            Side effects:
                -

            Possible Exception:
                -

            Restrictions:
                -
    """
    global MAX_ACTION
    MAX_ACTION = max_action
    amounts = get_bi_array()
    amounts = count_bi_amounts(amounts, int_data)

    probabilities = get_bi_array()
    for i in range(MAX_ACTION + 1):
        for j in range(MAX_ACTION + 1):
            probabilities[i][j] = (amounts[i][j] / (len(int_data)-1))

    seq_prob = get_bi_seq_prob(probabilities, int_data)

    return calculate_bi_probs(probabilities, seq_prob, int_data)
