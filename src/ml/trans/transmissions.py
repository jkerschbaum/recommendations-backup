from problog.logic import Term
from problog.program import PrologString
from problog.learning import lfi
import re
from copy import deepcopy
import os.path
import csv

MODEL = """"""
MAX_ACTION = 0
TERMS = []
INITIALISED = False
NUM_OF_RECS = 0
LAST_EVIDENCE = []
NEW_EVIDENCE = []

EVIDENCE_LIMIT = 200


def initialize(max_action, num_of_recs=5):
    """
            Initialises transmission model and connected functionality

            Keyword arguments:
                max_action: The number of the last action. Assumes 0-indexed,
                            so #Actions - 1 (non-optional)
                num_of_recs: The number of recommendations to be prepared
                             (default: 5)

            Return Values:
                True: Successful
                False: An error occurred at some point

            Side effects:
                -

            Possible Exception:
                -

            Restrictions:
                Only call once
    """
    global MAX_ACTION, MODEL, TERMS, INITIALISED, NUM_OF_RECS
    try:
        MAX_ACTION = max_action
        NUM_OF_RECS = num_of_recs
        lists = []
        # Create all possible lists of actions of length 1
        for i in range(MAX_ACTION + 1):
            newlist = [i]
            lists.append(newlist)
        # Create necessary Terms + build model
        # Note: This implements functionality that is not necessary for
        # lists of length 1 as such, for future use with longer lists
        for list1 in lists:
            append_string1 = ""
            for i in range(len(list1)):
                append_string1 += "{{{0}}}".format(i)
            append_string1 = append_string1.format(*list1)
            for list2 in lists:
                append_string2 = ""
                for i in range(len(list2)):
                    append_string2 += "{{{0}}}".format(i)
                append_string2 = append_string2.format(*list2)
                MODEL += "t(_)::transition_" + append_string1 + "_"\
                         + append_string2 + ". \n"
                TERMS.append(Term("transition_" + append_string1 + "_"
                                  + append_string2))
        # TERMS now lists the terms such that first comes every _0_X term,
        # then every _1_X term, and so on.
        # For X = {0, 1, ...., MAX_ACTION} in order
        read_evidence()
        INITIALISED = True
        return True
    except Exception:
        return False


def write_evidence():
    """
        Writes evidence for this model to file

        Keyword arguments:
            -

        Return Values:
            -

        Side effects:
            -

        Possible Exception:
            -

        Restrictions:
            Only has to be called once, but no negative effects if called more
            often
    """
    global LAST_EVIDENCE, NEW_EVIDENCE
    LAST_EVIDENCE = LAST_EVIDENCE + NEW_EVIDENCE
    with open('transition_examples.csv', newline='', encoding='utf-8',
              mode='w') as csvfile:
        writer = csv.writer(csvfile, delimiter=' ', quotechar='|',
                            quoting=csv.QUOTE_MINIMAL)
        for example in LAST_EVIDENCE:
            row = []
            for (term, value, cvalue) in example:
                row.append(term)
                row.append(value)
                row.append(cvalue)
                row.append('#')
            writer.writerow(row)


def read_evidence():
    """
               Reads saved evidence for this model from file

               Keyword arguments:
                    -

               Return Values:
                   -

               Side effects:
                   -

               Possible Exception:
                   -

               Restrictions:
                   Only has to be called once, but no negative effects if
                   called more often
       """
    global LAST_EVIDENCE
    evidence_loaded = []
    if os.path.isfile("./transition_examples.csv"):
        with open('transition_examples.csv', newline='', encoding='utf-8',
                  mode='r') as csvfile:
            reader = csv.reader(csvfile, delimiter=' ', quotechar='|')
            for row in reader:
                example = ', '.join(row)
                split_example = example.split('#')
                num_examples = len(split_example) - 1
                examples_row = []
                for i in range(num_examples):
                    ex = split_example[i]
                    if ex[0] == ',':
                        ex = ex[2:]
                    ex = ex[:len(ex) - 2]
                    ex_split = ex.split(',')
                    ex_split[1] = ex_split[1][1:]
                    ex_split[2] = ex_split[2][1:]
                    term = Term(ex_split[0])
                    boolean = True
                    if ex_split[1] == 'False':
                        boolean = False
                    cvalue = int(ex_split[2])
                    new_tuple = (term, boolean, cvalue)
                    examples_row.append(new_tuple)
                evidence_loaded.append(examples_row)
        LAST_EVIDENCE = evidence_loaded


def build_evidence(int_data):
    """
                Builds evidence list for use with Transmission Model

                Keyword arguments:
                    int_data: Data list of atomic actions represented as
                              integers (non-optional)

                Return Values:
                    evidence: list of evince tuples as required for LFI Problog

                Side effects:
                    -

                Possible Exception:
                    -

                Restrictions:
                    -
    """
    evidence = []
    sublists = []

    # Construct all sublists of int_data with length 2
    for i in range(len(int_data))[1:]:
        sublist = [int_data[i-1], int_data[i]]
        sublists.append(sublist)

    for sublist in sublists:
        first = sublist[0]
        second = sublist[1]
        example = []
        index = ((MAX_ACTION + 1) * first) + second
        example.append((TERMS[index], True, 0))
        evidence.append(example)

    return evidence


def extract(from_act, to_act, probs):
    """
                      Extracts transmission probability from from_act to to_act
                      from probs

                      Keyword arguments:
                          from_act: the first part of the identifier for the
                                    probability (non-optional)
                          to_act: the second part of the identifier for the
                                  probability (non-optional)
                          probs: the set of probabilities to be extracted from
                                 (non-optional)

                      Return Values:
                          prob: transmission probability from from_act to
                                to_act expressed as number

                      Side effects:
                          -

                      Possible Exception:
                          -

                      Restrictions:
                          -
    """
    global MAX_ACTION
    index = (from_act * (MAX_ACTION + 1)) + to_act
    prob_string = probs[index]
    prob = float(prob_string)
    return prob


def calculate_followup_probabilities(probs, int_data):
    """
                          Calculates the followup-probabilities for all
                          possible next actions

                          Keyword arguments:
                              probs: set of transition-probabilities
                                     (non-optional)

                          Return Values:
                              follow_probs: Probability of sequence continuing
                                            with specific next action

                          Side effects:
                              -

                          Possible Exception:
                              -

                          Restrictions:
                              -
    """
    global MAX_ACTION
    sublists = []
    if len(int_data) > EVIDENCE_LIMIT:
        int_data = int_data[(len(int_data) - EVIDENCE_LIMIT):]
    # Construct all sublists of int_data with length 2
    for i in range(len(int_data))[1:]:
        sublist = [int_data[i - 1], int_data[i]]
        sublists.append(sublist)
    sequence_prob = 10 ** len(int_data)
    for list1 in sublists:
        sequence_prob *= extract(list1[0], list1[1], probs)
    follow_probs = [0] * (MAX_ACTION + 1)
    last_action = int_data[len(int_data)-1]
    for i in range(len(follow_probs)):
        seq_prob = deepcopy(sequence_prob)
        seq_prob *= extract(last_action, i, probs)
        follow_probs[i] = seq_prob
    return follow_probs


def get_transmission_recs(int_data):
    """
                   Controller Function. Automates usage of the Transmission
                   Model

                   Keyword arguments:
                       int_data: Data list of atomic actions represented as
                                 integers (non-optional)

                   Return Values:
                       top_unique: List of ints that represent the possible
                                   next user actions as predicted
                                   by the transmission model

                   Side effects:
                       -

                   Possible Exception:
                       RuntimeError: Model was not initialised before function
                                     call is made

                   Restrictions:
                       Only call after initialize has been called once
    """
    global MODEL, INITIALISED, LAST_EVIDENCE, NEW_EVIDENCE
    if not INITIALISED:
        raise RuntimeError("Model not initialised")
    evidence = build_evidence(int_data)
    NEW_EVIDENCE = evidence
    evidence = evidence + LAST_EVIDENCE
    if len(evidence) > EVIDENCE_LIMIT:
        evidence = evidence[(len(evidence) - EVIDENCE_LIMIT):]
    score, weights, atoms, iteration, lfi_problem =\
        lfi.run_lfi(PrologString(MODEL), evidence)
    probs = re.findall(r"\d\.\d+", lfi_problem.get_model())
    follow_probs = calculate_followup_probabilities(probs, int_data)
    follow_probs_sorted = sorted(follow_probs)
    follow_probs_sorted.reverse()
    top = []
    for i in range(len(follow_probs_sorted)):
        index = follow_probs.index(follow_probs_sorted[i])
        top.append(index)
        if len(set(top)) == NUM_OF_RECS or (len(set(top)) == MAX_ACTION
                                            and MAX_ACTION < NUM_OF_RECS):
            break
    top_unique = []
    [top_unique.append(x) for x in top if x not in top_unique]
    return top_unique
