import src.data.atomic_actions as atomic_actions
import src.hc.hardcoded as hardcoded
import src.ml.learned as learned
import src.ml.met_mod.meta as meta
import src.ml.trans.transmissions as transmissions
from src.comm.recommendations_list import get_recs_from_hc_and_l_recs
from src.data.slaveThread import generate_thread
from src.data.translator import dsl_to_atomics, atomics_to_dsl

import threading
import time

NUM_OF_RECS = 0
RECS_BUFFER = []
DATA_UPDATED = False
CURRENT_METADATA = hardcoded.DataCarriage()

CONT_FLAGS = []
NEXT_EVENT = []
THREADS = []


def rec_init(num_of_recs=5):
    """
    Initialises all recommendation functionality

    Keyword arguments:
        num_of_recs: the number of recommendations to be prepared (default: 5)

    Return Values:
        True: Init succeeded
        False or None: Init failed

    Side Effects:
        -

    Possible Exceptions:
        -

    Restriction:
        Call only once, at setup
    """
    global DATA_UPDATED, NUM_OF_RECS
    max_action = 12
    hc_worked = hardcoded.init_hcr()
    learned_worked = learned.init_learned(max_action, num_of_recs)
    DATA_UPDATED = False
    NUM_OF_RECS = num_of_recs
    return not ((not hc_worked) or (not learned_worked))


def rec_new_data(dsl_string):
    """
    Takes in new (non-met_mod) data and updates the internal state

    Keyword arguments:
        dsl_string: dsl as entered by user, to be used for predictions
                    (non-optional)

    Return Values:
        True: Succeeded
        False or Init: Failed

    Side Effects:
        -

    Possible Exceptions:
        -

    Restrictions:
        -
    """
    global DATA_UPDATED
    try:
        translated = dsl_to_atomics(dsl_string)
        for action in translated:
            atomic_actions.action_performed(action)
        DATA_UPDATED = True
        meta.make_evidence(translated[len(translated) - 1])
        return True
    except (IndexError or KeyError or MemoryError or RuntimeError or
            SystemError or WindowsError):
        return False


def rec_get_recs():
    """
    Gets prepared Recs from the buffer, as prepared from last update

    Keyword arguments:
        -

    Return Values:
        ret_recs: List of Strings in DSL-Code-Format that represent predicted
        behaviour

    Side Effects:
        -

    Possible Exceptions:
        -

    Restrictions:
        -
    """
    global RECS_BUFFER, DATA_UPDATED
    rec_update()
    return RECS_BUFFER


def rec_empty_datacarriage():
    """
    Returns empty DataCarriage Object for collection and updating of Metadata
    variables

    Keyword arguments:
        -

    Return Values:
        dc: Empty DataCarriage Object for collection and updating of metadata

    Side Effects:
        -

    Possible Exceptions:
        -

    Restrictions:
        -
    """
    return hardcoded.DataCarriage()


def rec_new_metadata(dc, update=False):
    """
    Takes in new met_mod-data in the form of a DataCarriage Object and updates
    the internal state

    Keyword arguments:
        dc: DataCarriage Object used to draw new metadata from (non-optional)

    Return Values:
        True: Succeeded
        False or None: Failed

    Side Effects:
        -

    Possible Exceptions:
        -

    Restrictions:
        -
    """
    global CURRENT_METADATA, DATA_UPDATED
    try:
        if update:
            CURRENT_METADATA = hardcoded.update_dc(CURRENT_METADATA, dc)
            DATA_UPDATED = True
            return True
        else:
            CURRENT_METADATA = dc
            DATA_UPDATED = True
            return True
    except (IndexError or KeyError or MemoryError or RuntimeError or
            SystemError or WindowsError):
        return False


def get_current_metadata():
    """
        Returns CURRENT_METADATA

        Keyword arguments:
            -

        Return Values:
            CURRENT_METADATA

        Side Effects:
            -

        Possible Exceptions:
            -

        Restrictions:
            -
        """
    return CURRENT_METADATA


def get_data_updated():
    """
        Returns DATA_UPDATED

        Keyword arguments:
            -

        Return Values:
            DATA_UPDATED

        Side Effects:
            -

        Possible Exceptions:
            -

        Restrictions:
            -
        """
    return DATA_UPDATED


def rec_update():
    """
    Updates recommendations provided based on the current internal state
    (data + metadata)

    Keyword arguments:
        -

    Return Values:
        True: Succeeded
        False: Failed
        None: No data set to update from

    Side Effects:
        -

    Possible Exceptions:
        -

    Restrictions:
        -
    """
    global DATA_UPDATED, CURRENT_METADATA, RECS_BUFFER
    if not DATA_UPDATED:
        return None
    try:
        hardcoded_recs = []
        thread = generate_thread(hardcoded.get_recs_from_carriage,
                                 hardcoded_recs, CURRENT_METADATA)
        thread.start()
        learned_recs = learned.get_learned_recs(atomic_actions.data)

        thread.join()
        # TODO: Ensure this always works without raising Exceptions
        unified_recs = get_recs_from_hc_and_l_recs(hardcoded_recs,
                                                   learned_recs, NUM_OF_RECS)
        unified_recs_trans = []
        for i in range(len(unified_recs)):
            unified_recs_trans.append(atomics_to_dsl(unified_recs[i]))
        RECS_BUFFER = unified_recs_trans
        DATA_UPDATED = False
        return True
    except (IndexError or KeyError or MemoryError or RuntimeError
            or SystemError or WindowsError or ImportError
            or ModuleNotFoundError):
        return False


def start_auto_updater(func_timer, func_target):
    """
        Starts a new thread that automatically updates the recommendations
        every func_timer minutes and pushes them
        into func_target. func_target is assumed to be a list, and must
        implement the __setitem__ and append functions
        for this to work.

        For ideal functionality, func_target should be a single, empty list
        that is not assigned to, ever. At all.

        Keyword arguments:
            func_timer: (Whole) Number, representing the time in minutes
                        ideally elapsing between updates
            func_target: Caller-side variable (reference) to push updated recs
                         into.

        Return Values:
            id: Returns length of THREADS array.

        Side Effects:
            Changes func_target variables value at relatively consistent
            intervals

        Possible Exceptions:
            -

        Restrictions:
            -
        """
    global CONT_FLAGS, THREADS, NEXT_EVENT
    id = len(THREADS)
    CONT_FLAGS.append(True)
    offset = 60 * func_timer
    NEXT_EVENT.append(0)

    class BackgroundUpdaterThread(threading.Thread):
        def __init__(self, id, offset):
            threading.Thread.__init__(self)
            self.id = id
            self.offset = offset

        def run(self):
            global NEXT_EVENT
            while CONT_FLAGS[self.id]:
                NEXT_EVENT[self.id] = (time.time() + self.offset)
                set_recs = rec_get_recs()
                while len(func_target) < len(set_recs):
                    func_target.append(-1)
                while NEXT_EVENT[self.id] < time.time():
                    NEXT_EVENT[self.id] += self.offset
                time_to_next_event = NEXT_EVENT[self.id] - time.time()
                time.sleep(time_to_next_event)
                for i in range(len(func_target)):
                    func_target.__setitem__(i, set_recs[i])

    thread = BackgroundUpdaterThread(id, offset)
    THREADS.append(thread)
    thread.start()
    return id


def end_auto_updater(id):
    """
        Shuts down an auto-updater thread. The thread may perform up to one
        more full update after this is called.

        Keyword arguments:
            id: id of the thread to be killed

        Return Values:
            -

        Side Effects:
            -

        Possible Exceptions:
            -

        Restrictions:
            -
    """
    global CONT_FLAGS, THREADS
    CONT_FLAGS[id] = False
    THREADS[id].join()


def rec_on_shutdown():
    """
    Shutdown function. Cleans up various bits and pieces, updates long-term
    evidence storage

    Keyword arguments:
        -

    Return Values:
        -

    Side Effects:
        Changes .csv files used for long-term evidence storage

    Possible Exceptions:
        -

    Restrictions:
        Only call on shutdown
    """
    global CONT_FLAGS, THREADS
    transmissions.write_evidence()
    meta.write_evidence()
    # End all running side threads and wait for them to finish
    for i in range(len(CONT_FLAGS)):
        CONT_FLAGS[i] = False
    for i in THREADS:
        i.join()
