import copy
import math
import random
from src.data.atomic_actions import data, AtomicActions


def get_recs_from_hc_and_l_recs(hc_recs, l_recs, num_of_recs):
    """
        Returns list of recommendations containing hardcoded and learned recs.
        If - at the beginning of the session - there is not a lot of data
        to base learned recommendations on, more hardcoded recommendations
        are used. This changes with further usage by using a sigmoid function.

        Keyword arguments:
            hc_recs: List of lists of atomic actions of hardcoded recs.
            l_recs: List of integers of learned recs.

        Return Values:
            res_recs: List of lists of atomic actions.

        Side effects:
            -

        Possible Exception:
            -

        Restrictions:
            -
    """

    # Translate l_recs into atomic actions:
    l_recs = one_d_list_to_list_of_lists(l_recs)
    l_recs = int_to_atomic_actions(l_recs)

    # Remove duplicates from learned recs list
    l_recs, hc_recs = remove_duplicates(l_recs, hc_recs)

    # Define variables
    amount_possible_hc = len(hc_recs)
    amount_possible_l = len(l_recs)

    # Set the ratio of hardcoded and learned recs depending on amount of data
    # available (more data -> better learned recs -> more learned recs)
    usage_indicator = len(data)
    percent_learned = distribution(usage_indicator)
    amount_learned = math.floor(percent_learned * num_of_recs)
    amount_hardcoded = num_of_recs - amount_learned

    # Are there enough recommendations of each type?
    enough_recs_combined = True if (num_of_recs <= amount_possible_hc
                                    + amount_possible_l) else False
    enough_recs_hardcoded = True if (amount_hardcoded <= amount_possible_hc) \
        else False
    enough_recs_learned = True if (amount_learned <= amount_possible_l) \
        else False

    # If not enough recs of one type are available, fill with other recs if
    # possible
    if not enough_recs_hardcoded and enough_recs_combined:
        return not_enough_hc(hc_recs, amount_hardcoded, amount_possible_hc,
                             amount_learned, amount_possible_l, l_recs,
                             percent_learned)
    elif not enough_recs_learned and enough_recs_combined:
        return not_enough_learned(hc_recs, amount_hardcoded,
                                  amount_possible_hc, amount_learned,
                                  amount_possible_l,
                                  l_recs, percent_learned)
    elif not enough_recs_combined:
        return not_enough_combined(hc_recs, amount_hardcoded,
                                   amount_possible_hc, amount_learned,
                                   amount_possible_l,
                                   l_recs, percent_learned)
    elif (enough_recs_learned and enough_recs_hardcoded
          and enough_recs_combined):
        return enough_recs_available(hc_recs, amount_hardcoded,
                                     amount_possible_hc, amount_learned,
                                     amount_possible_l,
                                     l_recs, percent_learned)
    else:
        return [[AtomicActions.action_none] for _ in range(num_of_recs)]


def not_enough_hc(hc_recs, amount_hardcoded, amount_possible_hc,
                  amount_learned, amount_possible_l, l_recs,
                  percent_learned):
    """
        Returns list of recommendations containing hardcoded and learned recs.
        Because not enough hardcoded
        recommendations are available the algorithms fills the list with other
        learned recs.

        Keyword arguments:
            hc_recs: List of lists of atomic actions of hardcoded recs
            l_recs: List of lists of atomic actions of hardcoded recs
            amount_hardcoded: Amount of expected hardcoded recommendations
            amount_possible_hc: Maximum amount of hardcoded recommendations
                                that are available
            amount_learned: Amount of expected learned recommendations
            amount_possible_l: Maximum amount of learned recommendations that
                               are available
            percent_learned: Percentage of recommendations that should be
                             learned

        Return Values:
            res: List of lists of atomic actions.

        Side effects:
            -

        Possible Exception:
            -

        Restrictions:
            -
    """
    res = []
    difference = amount_hardcoded - amount_possible_hc
    index_l = get_index_of_random_recs(amount_learned + difference,
                                       amount_possible_l)
    res_l_recs = [l_recs[i] for i in index_l if i != -1]

    # Prioritize based on usage indicator
    if percent_learned <= 0.5:
        res.extend(hc_recs)
        res.extend(res_l_recs)
    else:
        res.extend(res_l_recs)
        res.extend(hc_recs)
    return res


def not_enough_learned(hc_recs, amount_hardcoded, amount_possible_hc,
                       amount_learned, amount_possible_l, l_recs,
                       percent_learned):
    """
        Returns list of recommendations containing hardcoded and learned recs.
        Because not enough learned
        recommendations are available the algorithms fills the list with other
        hardcoded recs.

        Keyword arguments:
            hc_recs: List of lists of atomic actions of hardcoded recs
            l_recs: List of lists of atomic actions of hardcoded recs
            amount_hardcoded: Amount of expected hardcoded recommendations
            amount_possible_hc: Maximum amount of hardcoded recommendations
                                that are available
            amount_learned: Amount of expected learned recommendations
            amount_possible_l: Maximum amount of learned recommendations that
                               are available
            percent_learned: Percentage of recommendations that should be
                             learned

        Return Values:
            res: List of lists of atomic actions.

        Side effects:
            -

        Possible Exception:
            -

        Restrictions:
            -
    """
    res = []
    difference = amount_learned - amount_possible_l
    index_hc = get_index_of_random_recs(amount_hardcoded + difference,
                                        amount_possible_hc)
    res_hc_recs = [hc_recs[i] for i in index_hc if i != -1]

    # Prioritize based on usage indicator
    if percent_learned <= 0.5:
        res.extend(res_hc_recs)
        res.extend(l_recs)
    else:
        res.extend(hc_recs)
        res.extend(res_hc_recs)
    return res


def not_enough_combined(hc_recs, amount_hardcoded, amount_possible_hc,
                        amount_learned, amount_possible_l, l_recs,
                        percent_learned):
    """
        Returns list of recommendations containing hardcoded and learned recs.
        Because not enough hardcoded and learned
        recommendations are available the algorithms fills the list with
        AtomicActions.action_none.

        Keyword arguments:
            hc_recs: List of lists of atomic actions of hardcoded recs
            l_recs: List of lists of atomic actions of hardcoded recs
            amount_hardcoded: Amount of expected hardcoded recommendations
            amount_possible_hc: Maximum amount of hardcoded recommendations
                                that are available
            amount_learned: Amount of expected learned recommendations
            amount_possible_l: Maximum amount of learned recommendations that
                               are available
            percent_learned: Percentage of recommendations that should be
                             learned

        Return Values:
            res: List of lists of atomic actions.

        Side effects:
            -

        Possible Exception:
            -

        Restrictions:
            -
    """
    to_fill = (amount_hardcoded + amount_learned - amount_possible_hc
               - amount_possible_l)
    res = []

    # Prioritize based on usage indicator
    if percent_learned <= 0.5:
        res.extend(hc_recs)
        res.extend(l_recs)
        for i in range(to_fill):
            res.append([AtomicActions(-1)])
    else:
        res.extend(l_recs)
        res.extend(hc_recs)
        for i in range(to_fill):
            res.append([AtomicActions(-1)])
    return res


def enough_recs_available(hc_recs, amount_hardcoded, amount_possible_hc,
                          amount_learned, amount_possible_l,
                          l_recs, percent_learned):
    """
        Returns list of recommendations containing hardcoded and learned recs
        in the case that there are enough
        hardcoded and enough learned recommendations available.

        Keyword arguments:
            hc_recs: List of lists of atomic actions of hardcoded recs
            l_recs: List of lists of atomic actions of hardcoded recs
            amount_hardcoded: Amount of expected hardcoded recommendations
            amount_possible_hc: Maximum amount of hardcoded recommendations
                                that are available
            amount_learned: Amount of expected learned recommendations
            amount_possible_l: Maximum amount of learned recommendations that
                               are available
            percent_learned: Percentage of recommendations that should be
                             learned

        Return Values:
            res: List of lists of atomic actions.

        Side effects:
            -

        Possible Exception:
            -

        Restrictions:
            -
    """
    res = []

    # If enough recs are available recommend random ones of both lists
    index_l = get_index_of_random_recs(amount_learned, amount_possible_l)
    index_hc = get_index_of_random_recs(amount_hardcoded, amount_possible_hc)

    # Get recs from indices
    res_l_recs = [l_recs[i] for i in index_l if i != -1]
    res_hc_recs = [hc_recs[i] for i in index_hc if i != -1]

    # Prioritize based on usage indicator
    if percent_learned <= 0.5:
        res.extend(res_hc_recs)
        res.extend(res_l_recs)
    else:
        res.extend(res_l_recs)
        res.extend(res_hc_recs)
    return res


def distribution(usage_indicator):
    """
        Returns a number between 0 and 1 depending on the usage_indicator.
        Higher values for higher usage_indicator.

        Keyword arguments:
            usage_indicator: Positive real number.

        Return Values:
            res: Evaluation of the function at usage_indicator.

        Side effects:
            -

        Possible Exception:
            -

        Restrictions:
            -
    """

    # Sigmoid function

    # Parameters:
    stretch_factor_1 = 0.02  # Higher: Slower increase
    shift_to_right = 70
    shift_upwards = 1 / 2

    # Calculate f(usage_indicator)
    res = ((math.e
            ** (stretch_factor_1 * (usage_indicator - shift_to_right)) - 1)
           / (2 * (1 + math.e ** (stretch_factor_1
                                  * (usage_indicator - shift_to_right))))
           + shift_upwards)
    return res


def remove_duplicates(list1, list2):
    """
        Returns the two input lists without duplicate entries over both lists.
        The duplicates are removed from the first lists.

        Keyword arguments:
            list1: List of lists of atomic actions.
            list2: List of lists of atomic actions..

        Return Values:
            list1: List of lists of atomic actions.
            list2: List of lists of atomic actions.

        Side effects:
            -

        Possible Exception:
            -

        Restrictions:
            -
    """
    # First, remove duplicates within each list
    res_list1 = []
    res_list2 = []
    for item1 in list1:
        if item1 not in res_list1:
            res_list1.append(item1)
    for item2 in list2:
        if item2 not in res_list2:
            res_list2.append(item2)
    list1 = res_list1
    list2 = res_list2

    # Second, remove duplicates that are in both lists
    # Generate copy to iterate over
    list2_deepcopy = copy.deepcopy(list2)
    for item in list2_deepcopy:
        if item in list1:
            list1.remove(item)
    return list1, list2


def get_index_of_random_recs(how_many, amount_possible):
    """
        Returns a list of indices of an array without duplicates.

        Keyword arguments:
            how_many: How many indices are wanted.
            amount_possible: How many indices are there.

        Return Values:
            index: List of indices

        Side effects:
            -

        Possible Exception:
            -

        Restrictions:
            -
    """
    if amount_possible < how_many:
        return [-1] * how_many
    else:
        index = []
        while how_many > 0:
            new_index = random.randint(0, amount_possible - 1)
            if new_index not in index:
                index.append(new_index)
                how_many -= 1
        return index


def int_to_atomic_actions(recs):
    """
        Converts a list of lists of integers into a list of lists of atomic
        actions.


        Keyword arguments:
            recs: List of lists of recommendations as integers.

        Return Values:
            recs_translated: List of lists of atomic actions.

        Side effects:
            -

        Possible Exception:
            ValueError: Integers have to be valid AtomicActions.

        Restrictions:
            -
    """
    recs_translated = []
    for rec in recs:
        temp = []
        for r in rec:
            temp.append(AtomicActions(r))
        recs_translated.append(temp)
    return recs_translated


def one_d_list_to_list_of_lists(one_d):
    """
        Converts a one dimensional list of integers into a list of 
        lists of integers.


        Keyword arguments:
            one_d: List of integers.

        Return Values:
            list_of_lists: List of lists of the same integers.

        Side effects:
            -

        Possible Exception:
            -

        Restrictions:
            -
    """

    # If empty return [[]], otherwise convert.
    if not one_d:
        return [[]]
    else:
        list_of_lists = []
        for i in one_d:
            list_of_lists.append([i])

        return list_of_lists
